from configparser import ConfigParser

import click
import cv2
import numpy as np

from src.constant.visualization import *
from src.detection import get_detector
from src.utils.demo import get_stream
from src.utils.geometry import is_inside_polygon
from src.utils.visualization import draw_boxes


@click.command()
@click.option("--config_path", required=True, type=str)
@click.option("--source", required=True, type=str)
@click.option("--output", required=True, type=str)
def run(config_path, source, output):
    cfg = ConfigParser()
    cfg.read(config_path)

    detector = get_detector("M2_SSD", cfg, "onnx")
    stream = get_stream(source, "online")

    frame = stream.get_frame()
    w, h = frame.shape[:2]
    stream.start()

    video_fps = 3
    four_cc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
    video_writer = cv2.VideoWriter(output, four_cc, video_fps, (h, w))
    count = 0

    while True:
        ret, frame = stream.read()

        # if count == 50:
        #     break

        if frame is None:
            break

        count += 1
        boxes, scores = detector.detect_image(frame, confidence=0.1)

        tmp_boxes, tmp_scores = [], []

        for i, box in enumerate(boxes):
            centroid = [(box[0] + box[2]) / 2, (box[1] + box[3]) / 2]
            if is_inside_polygon(interest_of_region, centroid):
                if is_inside_polygon(
                        [[outlier_regions[0], outlier_regions[1]], [outlier_regions[2], outlier_regions[1]],
                         [outlier_regions[2], outlier_regions[3]], [outlier_regions[0], outlier_regions[3]]], centroid):
                    continue

                is_box = True
                for point in outlier_points:
                    if is_inside_polygon([[box[0], box[1]], [box[2], box[1]], [box[2], box[3]], [box[0], box[3]]],
                                         point):
                        is_box = False
                if is_box:
                    tmp_boxes.append(box)
                    tmp_scores.append(scores[i])

        boxes = np.array(tmp_boxes)
        scores = np.array(tmp_scores)

        idx = scores > 0.12
        frame = draw_boxes(frame, boxes[idx], scores[idx])

        video_writer.write(frame)

    video_writer.release()
    stream.stop()


if __name__ == "__main__":
    run()
