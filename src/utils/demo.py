import datetime
import os
import queue
from threading import Thread

import cv2

from src.utils.visualization import PolygonDrawer


class FPS:
    def __init__(self):
        self._start = None
        self._end = None
        self.num_frames = 0

    def start(self):
        self._start = datetime.datetime.now()

    def get_start(self):
        return self._start

    def stop(self):
        self._end = datetime.datetime.now()

    def update(self):
        self.num_frames += 1

    def elapsed(self):
        return (self._end - self._start).total_seconds()

    def fps(self):
        return self.num_frames / self.elapsed()


class WebCamVideoStream:
    def __init__(self, src, image_endswith=".jpg"):
        self.stream = cv2.VideoCapture(src)
        self.grabbed, self.frame = self.stream.read()
        self.stopped = False

    def get_frame(self):
        return self.frame

    def start(self):
        Thread(target=self.update, args=()).start()

    def update(self):
        while True:
            if self.stopped:
                return
            self.grabbed, self.frame = self.stream.read()

    def read(self):
        # return the frame most recently read
        return -1, self.frame

    def stop(self):
        # indicate that the thread should be stopped
        self.stopped = True
        self.stream.release()


class VideoStream:
    def __init__(self, src, image_endswith=".jpg"):
        if os.path.isdir(src):
            self.stream = cv2.VideoCapture(f"{src}/%06d{image_endswith}", cv2.CAP_IMAGES)
        else:
            self.stream = cv2.VideoCapture(src)
        grabbed, self.frame = self.stream.read()
        self.stopped = False
        self.q = queue.Queue()
        self.q.put(self.frame)

    def get_frame(self):
        return self.frame

    def start(self):
        Thread(target=self.update, args=()).start()

    def update(self):
        while True:
            if self.stopped:
                return
            grabbed, frame = self.stream.read()
            if grabbed == -1:
                self.stop()
                continue
            self.q.put(frame)

    def read(self):
        return -1, self.q.get()

    def stop(self):
        self.stopped = True
        self.stream.release()

    def is_stop(self):
        return self.q.empty()


class OnlineStream:
    def __init__(self, src, image_endswith=".jpg"):
        self.frame_ids = None
        if os.path.isdir(src):
            self.stream = cv2.VideoCapture(f"{src}/%06d{image_endswith}", cv2.CAP_IMAGES)
            frame_ids = os.listdir(src)
            self.frame_ids = [frame_id.replace(image_endswith, "") for frame_id in frame_ids if
                              frame_id.endswith(image_endswith)]
        else:
            self.stream = cv2.VideoCapture(src)

        self.image_endswith = image_endswith
        self.frame_counter = 0
        if self.frame_ids:
            self.frame = cv2.imread(self.frame_ids[self.frame_counter])
        else:
            _, self.frame = self.stream.read()
            self.stream = cv2.VideoCapture(src)

    def get_frame(self):
        return self.frame

    def start(self):
        pass

    def read(self):
        grabbed, frame = self.stream.read()

        if grabbed == -1:
            return -1, None

        if self.frame_ids:
            frame_id = self.frame_ids[self.frame_counter]
        else:
            frame_id = -1
        self.frame_counter += 1

        return frame_id, frame

    def stop(self):
        self.stream.release()


def get_crop_region(frame):
    pd = PolygonDrawer("Draw polygon")
    points = []

    is_redraw = True

    while is_redraw:
        frame = pd.run(frame)
        points = pd.points
        while True:
            cv2.imshow('draw_rectangle', frame)
            k = cv2.waitKey(1) & 0xFF
            if k == ord('q'):
                cv2.destroyAllWindows()
                is_redraw = False
                break
    cv2.destroyAllWindows()
    return points


def get_stream(source, tracking_type="offline"):
    if tracking_type == "offline":
        if type(source) != str:
            stream = WebCamVideoStream(src=int(source))
        else:
            stream = VideoStream(source)
    else:
        stream = OnlineStream(source)
    return stream
