import math

import numpy as np


def onSegment(p, q, r):
    if max(p[0], r[0]) >= q[0] >= min(p[0], r[0]):
        if max(p[1], r[1]) >= q[1] >= min(p[1], r[1]):
            return True
    return False


def orientation(p, q, r):
    val = (q[1] - p[1]) * (r[0] - q[0]) - (q[0] - p[0]) * (r[1] - q[1])
    if val == 0: return 0
    if val > 0: return 1
    return 2


def poly_area(poly):
    x = poly[:, 0]
    y = poly[:, 1]
    return 0.5 * np.abs(np.dot(x, np.roll(y, 1)) - np.dot(y, np.roll(x, 1)))


def do_intersect(p1, q1, p2, q2):
    o1 = orientation(p1, q1, p2)
    o2 = orientation(p1, q1, q2)
    o3 = orientation(p2, q2, p1)
    o4 = orientation(p2, q2, q1)

    if o1 != o2 and o3 != o4:
        return True
    if o1 == 0 and onSegment(p1, p2, q1):
        return True
    if o2 == 0 and onSegment(p1, q2, q1):
        return True
    if o3 == 0 and onSegment(p2, p1, q2):
        return True
    if o4 == 0 and onSegment(p2, q1, q2):
        return True
    return False


def is_inside_polygon(polygon, p):
    n = len(polygon)
    if n < 3:
        return False
    extreme = [-99999999, p[1]]
    count, i = 0, 0
    while True:
        next_ = (i + 1) % n
        if do_intersect(polygon[i], polygon[next_], p, extreme):
            if orientation(polygon[i], p, polygon[next_]) == 0:
                return onSegment(polygon[i], p, polygon[next_])
            count += 1
        i = next_
        if i == 0:
            break
    return count & 1


def get_angle(v1, v2):
    d = (v1[0] * v2[0] + v1[1] * v2[1])
    e1 = math.sqrt(v1[0] * v1[0] + v1[1] * v1[1])
    e2 = math.sqrt(v2[0] * v2[0] + v2[1] * v2[1])

    d = d / (e1 * e2)
    pi = 3.14159
    return (180 / pi) * math.acos(d)


def get_intersect(A, B, C, D):
    a1 = B[1] - A[1]
    b1 = A[0] - B[0]
    c1 = a1 * (A[0]) + b1 * (A[1])

    a2 = D[1] - C[1]
    b2 = C[0] - D[0]
    c2 = a2 * (C[0]) + b2 * (C[1])

    determinant = a1 * b2 - a2 * b1

    if determinant == 0:
        return None
    else:
        x = (b2 * c1 - b1 * c2) / determinant
        y = (a1 * c2 - a2 * c1) / determinant
        return [x, y]


def retrieve_inside_boxes(polygons, boxes):
    res_indices = []
    remaining_indices = []
    for i, box in enumerate(boxes):
        is_belong = False
        for polygon in polygons:
            if is_inside_polygon(polygon, [box[0] + box[2] / 2, box[1] + box[3] / 2]):
                is_belong = True
                break
        if is_belong:
            res_indices.append(i)
        else:
            remaining_indices.append(i)
    return res_indices, remaining_indices


def region_belong(polygons, point):
    for i, polygon in enumerate(polygons):
        if is_inside_polygon(polygon, point):
            return i
    return -1


def unit_vector(vector):
    """ Returns the unit vector of the vector.  """
    return vector / np.linalg.norm(vector)


def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'::
        >>> angle_between((1, 0, 0), (0, 1, 0))
        1.5707963267948966
        >>> angle_between((1, 0, 0), (1, 0, 0))
        0.0
        >>> angle_between((1, 0, 0), (-1, 0, 0))
        3.141592653589793
    """
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))


def is_between_lines(point, line1, line2):
    temp1 = orientation(line1[0], line1[1], point)
    temp2 = orientation(line2[0], line2[1], point)
    if temp1 == 2 and temp2 == 1:
        return True
    return False
