import time
from logging import getLogger
from tabulate import tabulate

logger = getLogger(__name__)


class Timer:
    def __init__(self, log_name=""):
        self._name = log_name
        self._data = []
        self._current_time = 0
        self._current_task = ""
        self._is_running = False

    def start(self, task_name=""):
        self._current_time = time.time()
        self._current_task = task_name
        self._is_running = True

    def stop(self):
        self._data.append([self._current_task, (time.time() - self._current_time)*1000])
        self._is_running = False

    def timing_show(self):
        logger.info("\n" + tabulate(self._data, headers=("Task", "Running time (ms)")))


if __name__ == "__main__":
    pass
