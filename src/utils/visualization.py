import os
import time

import cv2

from src.utils.geometry import is_inside_polygon

CANVAS_SIZE = (600, 800)
FINAL_LINE_COLOR = (255, 255, 255)
WORKING_LINE_COLOR = (127, 127, 127)


class PolygonDrawer(object):

    def __init__(self, window_name, is_test=True):
        self.window_name = window_name
        self.done = False
        self.points = []

    def on_mouse(self, event, x, y, buttons, user_params):
        if self.done:
            return

        if event == cv2.EVENT_LBUTTONDOWN:
            self.points.append((x, y))
        elif event == cv2.EVENT_RBUTTONDOWN:
            self.done = True

    def run(self, image):
        cv2.namedWindow(self.window_name)
        cv2.imshow(self.window_name, image)
        cv2.waitKey(1)
        cv2.setMouseCallback(self.window_name, self.on_mouse)
        canvas = image

        while (not self.done):
            # This is our drawing loop, we just continuously draw new images
            # and show them in the named window
            if (len(self.points) > 0):
                # Draw all the current polygon segments
                # cv2.polylines(canvas, np.array([self.points]), True, FINAL_LINE_COLOR, 1)
                if len(self.points) >= 2:
                    cv2.line(canvas, (int(self.points[-1][0]), int(self.points[-1][1])),
                             (int(self.points[-2][0]), int(self.points[-2][1])), (0, 255, 0), 2)
                # And  also show what the current segment would look like
            # Update the window
            cv2.imshow(self.window_name, canvas)
            # And wait 50ms before next iteration (this will pump window messages meanwhile)
            if cv2.waitKey(50) == 27:  # ESC hit
                cv2.line(canvas, (int(self.points[-1][0]), int(self.points[-1][1])),
                         (int(self.points[0][0]), int(self.points[0][1])), (0, 255, 0), 2)
                self.done = True

            # User finised entering the polygon points, so let's make the final drawing
        # of a filled polygon
        # if (len(self.points) > 0):
        #     cv2.fillPoly(canvas, np.array([self.points]), FINAL_LINE_COLOR)
        # And show it
        # cv2.imshow(self.window_name, canvas)
        # # Waiting for the user to press any key
        # cv2.waitKey()
        #
        # cv2.destroyWindow(self.window_name)
        return canvas

    def pointInPolygon(self, canvas, point, is_display=False):
        cv2.circle(canvas, (int(point[0]), int(point[1])), 4, (0, 255, 0), -1)
        text = "Out"
        if is_inside_polygon(self.points, point) == 1:
            text = "In"
        cv2.putText(canvas, text, (0, 100), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

        cv2.imshow(self.window_name, canvas)
        # Waiting for the user to press any key
        cv2.waitKey()
        cv2.destroyWindow(self.window_name)
        return canvas


def draw_boxes(image, boxes, scores, is_plot=False, target_size=None, saving_file=None):
    for i, box in enumerate(boxes):
        box = boxes[i]
        cv2.rectangle(image, (int(box[0]), int(box[1])), (int(box[2]), int(box[3])), (255, 0, 0), 1)
        cv2.putText(image, f'{round(scores[i], 2)}',
                    (int(box[0]), int(box[1] + 20)), cv2.FONT_ITALIC, 0.3,
                    (0, 255, 0), 1)

    if target_size:
        image = cv2.resize(image, target_size)

    if is_plot:
        cv2.imshow("img", image)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

    if saving_file:
        cv2.imwrite(saving_file, image)

    return image


def new_draw_boxes(image, detections, tracker, is_plot=False, target_size=None, saving_file=None):
    for track in tracker.tracks:
        if track.is_confirmed() and track.time_since_update > 1:
            continue
        bbox = track.to_tlbr()
        cv2.rectangle(image, (int(bbox[0]), int(bbox[1])),
                      (int(bbox[2]), int(bbox[3])), (255, 255, 255),
                      2)
        cv2.putText(image, str(track.track_id),
                    (int(bbox[0]), int(bbox[1])), 0, 5e-3 * 200,
                    (0, 255, 0), 2)

        centroid = (bbox[0] + bbox[2]) / 2, (bbox[1] + bbox[3]) / 2
        text = "ID {}".format(track.track_id)
        cv2.putText(image, text, (int(centroid[0]), int(centroid[1])), 0, 5e-3 * 200,
                    (0, 255, 0), 2)

    for detection in detections:
        bbox = detection.to_tlbr()
        cv2.rectangle(image, (int(bbox[0]), int(bbox[1])),
                      (int(bbox[2]), int(bbox[3])), (255, 0, 0), 2)

    return image


def track_video(detector, tracker, streams, target_size=(1280, 720), output_video=None, output_file=None):
    total_time = 0.0
    video_writer = None
    result_writer = None

    if output_video is not None:
        os.makedirs(os.path.dirname(output_video), exist_ok=True)
        four_cc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
        video_writer = cv2.VideoWriter(output_video, four_cc, 20, target_size)

    if output_file is not None:
        os.makedirs(os.path.dirname(output_file), exist_ok=True)
        result_writer = open(output_file, "w")

    tracking_res = []
    id_map = {}

    for i, image_file in enumerate(streams):
        start = time.time()

        boxes, scores = detector.detect_image(image_file)
        image = cv2.imread(image_file)

        boxes[:, 2:] = boxes[:, 2:] - boxes[:, :2]

        detections = tracker.update(image, boxes, scores)
        total_time += time.time() - start

        frame_id = int(image_file.split("/")[-1].replace(".jpg", ""))

        for detection in detections:
            tracking_res.append(
                [frame_id, int(detection[-1]), detection[0], detection[1],
                 detection[2], detection[3]])

        if detections.shape[0] == 0:
            continue

        if video_writer:
            ids = detections[:, -1]
            boxes = detections[:, :4]
            boxes[:, 2:] = boxes[:, 2:] + boxes[:, :2]
            image = draw_boxes(image, boxes, ids)
            image = cv2.resize(image, target_size)
            video_writer.write(image)
        # else:
        #     tracks = tracker.get_tracks()
        #     for track in tracks:
        #         if track.is_confirmed() and track.time_since_update > 1:
        #             continue
        #
        #         box = track.to_tlbr()
        #
        #         # if id_map.get(int(track.track_id), None) is None:
        #         #     id_map[int(track.track_id)] = len(id_map) + 1
        #
        #         tracking_res.append(
        #             [frame_id, int(track.track_id), box[0], box[1],
        #              box[2], box[3]])

    if video_writer:
        video_writer.release()

    if result_writer:
        for line in tracking_res:
            print(
                f"{int(line[0])},{int(line[1])},{int(line[2])},{int(line[3])},"
                f"{int(line[4])},{int(line[5])},1,-1,-1,-1",
                file=result_writer)

        result_writer.close()

    print("Total Tracking took: %.3f for %d frames or %.1f FPS" % (
        total_time, len(streams), len(streams) / total_time))


def draw_polygon(frame, poly):
    for j in range(len(poly)):
        cv2.line(frame, (poly[j % len(poly)][0], poly[j % len(poly)][1]),
                 (poly[(j + 1) % len(poly)][0], poly[(j + 1) % len(poly)][1]), (0, 255, 0), 1)
    return frame
