from src.utils.geometry import is_inside_polygon


def count_in_region(regions, boxes):
    res = [0] * len(regions)

    for i, region in enumerate(regions):
        for box in boxes:
            centroid = [(box[0] + box[2]) / 2, (box[1] + box[3]) / 2]
            if is_inside_polygon(region, centroid):
                res[i] += 1
    return res


def analyze_track(customer_regions, counter_regions, boxes):
    is_in_counter = count_in_region(counter_regions, boxes)
    n_customers = count_in_region(customer_regions, boxes)
    return is_in_counter, n_customers
