import operator
import time
from typing import Dict

from tabulate import tabulate


def sort_map_by_value(m: Dict, reverse=True):
    return sorted(m.items(), key=operator.itemgetter(1), reverse=reverse)


def remove_alone_node(g):
    """
    Remove all node which not connect to any node in graph
    :param g: networkx Graph
    """
    remove_nodes = []
    for node in g.nodes:
        if len(list(g.neighbors(node))) == 0:
            remove_nodes.append(node)

    for node in remove_nodes:
        g.remove_node(node)


def get_batch_iterator(iter, bsize=100):
    batch = []

    for x in iter:
        batch.append(x)

        if len(batch) > bsize:
            yield batch
            batch = []

    yield batch


class StopWatch:
    """
    Stopwatch inspired by Spring Stopwatch
    """

    def __init__(self):
        self.is_running = False
        self.data = []
        self.total = 0.
        self.current_start_time = 0.
        self.current_task = -1

    def start(self, task_id: str):
        """
        Starting benchmark a task with name as task_id
        """

        if self.is_running:
            raise ValueError("Other task is running")

        self.current_start_time = time.time()
        self.current_task = task_id
        self.is_running = True

    def stop(self):
        """
        Stop current task
        """

        if not self.is_running:
            raise ValueError("No task is running")

        stop_time = time.time()
        duration = stop_time - self.current_start_time

        self.data.append([self.current_task, duration])
        self.is_running = False
        self.total += duration

    def pretty(self):
        self.data.append(['total', self.total])
        return tabulate(self.data, headers=['task', 'duration'], tablefmt='orgtbl')
