import numpy as np


def cpu_nms(boxes, scores, iou_threshold):
    """Pure Python NMS baseline."""
    x1 = boxes[:, 0]
    y1 = boxes[:, 1]
    x2 = boxes[:, 2]
    y2 = boxes[:, 3]

    areas = (x2 - x1 + 1) * (y2 - y1 + 1)
    order = scores.argsort()[::-1]

    keep = []
    while order.size > 0:
        i = order[0]
        keep.append(i)
        xx1 = np.maximum(x1[i], x1[order[1:]])
        yy1 = np.maximum(y1[i], y1[order[1:]])
        xx2 = np.minimum(x2[i], x2[order[1:]])
        yy2 = np.minimum(y2[i], y2[order[1:]])

        w = np.maximum(0.0, xx2 - xx1 + 1)
        h = np.maximum(0.0, yy2 - yy1 + 1)
        inter = w * h
        ovr = inter / (areas[i] + areas[order[1:]] - inter)

        indices = np.where(ovr <= iou_threshold)[0]
        order = order[indices + 1]

    return keep


def cpu_soft_nms(boxes, scores, iou_threshold=0.3, method='linear', sigma=0.5, score_thr=0.001):
    if method not in ('linear', 'gaussian', 'greedy'):
        raise ValueError('method must be linear, gaussian or greedy')

    x1 = boxes[:, 0]
    y1 = boxes[:, 1]
    x2 = boxes[:, 2]
    y2 = boxes[:, 3]

    areas = (x2 - x1 + 1) * (y2 - y1 + 1)
    # expand dets with areas, and the second dimension is
    # x1, y1, x2, y2, area
    boxes = np.concatenate((boxes, areas[:, None]), axis=1)
    # scores

    retained_box = []
    retained_score = []
    while boxes.size > 0:
        max_idx = np.argmax(scores)
        boxes[[0, max_idx], :] = boxes[[max_idx, 0], :]
        scores[[0, max_idx]] = scores[[max_idx, 0]]
        retained_box.append(boxes[0, :-1])
        retained_score.append(scores[0])

        xx1 = np.maximum(boxes[0, 0], boxes[1:, 0])
        yy1 = np.maximum(boxes[0, 1], boxes[1:, 1])
        xx2 = np.minimum(boxes[0, 2], boxes[1:, 2])
        yy2 = np.minimum(boxes[0, 3], boxes[1:, 3])

        w = np.maximum(xx2 - xx1 + 1, 0.0)
        h = np.maximum(yy2 - yy1 + 1, 0.0)
        inter = w * h
        iou = inter / (boxes[0, 4] + boxes[1:, 4] - inter)

        if method == 'linear':
            weight = np.ones_like(iou)
            weight[iou > iou_threshold] -= iou[iou > iou_threshold]
        elif method == 'gaussian':
            weight = np.exp(-(iou * iou) / sigma)
        else:  # traditional nms
            weight = np.ones_like(iou)
            weight[iou > iou_threshold] = 0

        scores[1:] *= weight
        retained_idx = np.where(scores[1:] >= score_thr)[0]
        boxes = boxes[retained_idx + 1, :]
        scores = scores[retained_idx + 1]

    # return np.vstack(retained_box), np.vstack(retained_score)
    return retained_box, retained_score


if False:
    # if torch.cuda.is_available():
    try:
        # from ext.kernel_nms import gpu_nms
        #
        # _nms = gpu_nms.nms
        print("torch_nms")
        import torch_extension

        _nms = torch_extension.nms
    except ImportError:
        print("kernel_nms")
        from ext.kernel_nms.gpu_nms import gpu_nms
        _nms = gpu_nms
        # _nms = torchvision.ops.boxes.nms
else:
    _nms = cpu_nms
    # _nms = torchvision.ops.boxes.nms


def boxes_nms(boxes, scores, nms_threshold, max_objects=-1):
    """ Performs non-maximum suppression, run on GPU or CPU according to
    boxes's device.
    Args:
        boxes(Tensor): `xyxy` mode boxes, use absolute coordinates(or relative coordinates), shape is (n, 4)
        scores(Tensor): scores, shape is (n, )
        nms_threshold(float): thresh
        max_objects (int): if > 0, then only the top max_proposals are kept  after non-maximum suppression
    Returns:
        indices kept.
    """
    keep = _nms(boxes, scores, nms_threshold)
    # keep = _nms(detections, nms_threshold)
    if max_objects > 0:
        keep = keep[:max_objects]
    return keep


def custom_nms(boxes, scores, nms_threshold, max_objects=-1):
    boxes, scores = cpu_soft_nms(boxes, scores, nms_threshold, method='gaussian')
    if max_objects > 0:
        boxes = boxes[:max_objects]
        scores = scores[:max_objects]
    return np.array(boxes), np.array(scores)


__all__ = ["boxes_nms", "custom_nms"]

if __name__ == "__main__":
    boxes = np.array([[100, 100, 210, 210, 0.72],
                      [250, 250, 420, 420, 0.8],
                      [220, 220, 320, 330, 0.92],
                      [100, 100, 210, 210, 0.72],
                      [230, 240, 325, 330, 0.81],
                      [220, 230, 315, 340, 0.9]], dtype=np.float32)
    scores = np.array([0.72, 0.8, 0.92, 0.72, 0.81, 0.9], dtype=np.float32)

    print('greedy result:')
    print(boxes[cpu_nms(boxes, scores, 0.7)])
    print('soft nms result:')
    print(cpu_soft_nms(boxes, scores, method='gaussian'))
