import logging
import os

import motmetrics as mm

logger = logging.getLogger(__name__)


def compare_data_frame(gt, ts):
    print(gt, ts)

    sequences = []
    accuracies = []

    for seq, acc in ts.items():
        if seq in gt:
            accuracies.append(mm.utils.compare_to_groundtruth(gt[seq], acc, 'iou', distth=0.5))
            sequences.append(seq)
        else:
            logger.warning(f"No ground truth {seq} is being provided")

    return accuracies, sequences


def benchmark_mAP(gt_folder, ts_folder, tracker, output):
    gt_seq = os.listdir(gt_folder)
    ts_seq = os.listdir(ts_folder)

    print(gt_seq)

    gt = {seq: mm.io.loadtxt(os.path.join(gt_folder, seq, "gt/gt.txt")) for seq in gt_seq}
    ts = {seq: mm.io.loadtxt(os.path.join(ts_folder, seq, "output.txt")) for seq in ts_seq}

    mh = mm.metrics.create()
    accuracies, sequences = compare_data_frame(gt, ts)

    logger.info("Running multi tracking benchmark")

    summary = mh.compute_many(accuracies, names=sequences, metrics=mm.metrics.motchallenge_metrics,
                              generate_overall=True)

    with open(output, 'a+') as f:
        f.write('---------------------------------------------------------------------------------------------')
        f.write('\r\n')
        f.write(tracker)
        f.write('\r\n')
        f.write(mm.io.render_summary(summary, formatters=mh.formatters, namemap=mm.io.motchallenge_metric_names))
        f.write('\r\n')
        f.write('---------------------------------------------------------------------------------------------')
        f.write('\r\n')
