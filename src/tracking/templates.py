from abc import ABC

import cv2
import numpy as np

from src.tracking.modules.assignment import linear_assignment
from src.tracking.modules.optical import getFeatures, estimateAllTranslation, applyGeometricTransformation, \
    bbox_transform
from src.tracking.modules.selector.local_based.visual_tracker import VisualTracker


class TrackerTemplate(object):
    def __init__(self, *args, **kwargs):
        pass

    def update(self, boxes, scores, image, *args, **kwargs):
        pass

    @staticmethod
    def set_detection_needed(value):
        linear_assignment.is_tracker_in_low_prob = value


class OpticalSortTemplate(TrackerTemplate, ABC):
    def __init__(self, cfg, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.previous_image = None

    def _optical(self, image, boxes):
        """
        :param image:
        :param boxes: xywh
        :return:
        """
        if self.previous_image is None:
            self.previous_image = image
            return boxes

        i, n_trajectories = 0, len(boxes)
        curr_boxes = np.empty((n_trajectories, 4, 2), dtype=float)
        for box in boxes:
            x_min, y_min, w, h = int(box[0]), int(box[1]), int(box[2]), int(box[3])
            curr_boxes[i, :, :] = np.array([
                [x_min, y_min], [x_min + w, y_min],
                [x_min, y_min + h], [x_min + w, y_min + h]
            ]).astype(float)
            i = i + 1

        start_xs, start_ys = getFeatures(cv2.cvtColor(image, cv2.COLOR_RGB2GRAY), curr_boxes, use_shi=True)

        new_xs, new_ys = estimateAllTranslation(start_xs, start_ys, self.previous_image, image)
        xs, ys, new_boxes = applyGeometricTransformation(start_xs, start_ys, new_xs, new_ys, curr_boxes)

        self.previous_image = image
        boxes = bbox_transform(new_boxes, boxes)
        return boxes

    def _directly_predict(self, image, boxes):
        """

        :param image: shape
        :param boxes: xyxy
        :return: xyxy new boxes
        """
        return self._optical(image, boxes)


class CorrFilterSortTemplate(TrackerTemplate, ABC):
    def __init__(self, cfg, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.visual_tracker = VisualTracker(cfg["visual_type"])
        self.first = True

    def init(self, image, boxes):
        self.visual_tracker.init(image, boxes)

    def _deformable(self, image):
        return self.visual_tracker.update(image)

    def _directly_predict(self, image, boxes):
        if self.first:
            self.init(image, boxes)
            self.first = False
            return boxes

        boxes_temp = self._deformable(image)
        return boxes_temp
