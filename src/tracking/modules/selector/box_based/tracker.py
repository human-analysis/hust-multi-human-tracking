import numpy as np

from src.tracking.modules.selector.common.kalman_tracker import KalmanBoxTracker
from src.tracking.modules.utils import associate_detections_to_tracks


class SortTracker(object):
    def __init__(self, max_age=30, min_hits=3, use_facial_model=False):
        """
        Sets key parameters for SORT
        """
        self.max_age = max_age
        self.min_hits = min_hits
        self.tracks = []
        self.frame_count = 0

        self.track_object = KalmanBoxTracker
        # if use_facial_model:
        #     self.track_object = CorrelationTracker

    def update(self, detections, image=None):
        """
        Params:
          detections - a numpy array of detections in the format [[x1,y1,x2,y2,score],[x1,y1,x2,y2,score],...]
        Requires: this method must be called once for each frame even with empty detections.
        Returns the a similar array, where the last column is the object ID.

        NOTE: The number of objects returned may differ from the number of detections provided.
        """
        self.frame_count += 1
        # get predicted locations from existing tracks.
        tracks = np.zeros((len(self.tracks), 5))
        to_del = []
        ret = []
        for t, trk in enumerate(tracks):
            pos = self.tracks[t].predict()
            trk[:] = [pos[0], pos[1], pos[2], pos[3], 0]
            if np.any(np.isnan(pos)):
                to_del.append(t)
        tracks = np.ma.compress_rows(np.ma.masked_invalid(tracks))
        for t in reversed(to_del):
            self.tracks.pop(t)

        matched, unmatched_detections, unmatched_tracks = associate_detections_to_tracks(detections, tracks)

        # update matched tracks with assigned
        for t, trk in enumerate(self.tracks):
            if t not in unmatched_tracks:
                d = matched[np.where(matched[:, 1] == t)[0], 0]
                trk.update(detections[d, :][0])
                trk.confirmed = True
            else:
                trk.confirmed = False

        # create and initialise new tracks for unmatched detections
        for i in unmatched_detections:
            trk = self.track_object(detections[i, :], image)
            self.tracks.append(trk)
        i = len(self.tracks)
        for trk in reversed(self.tracks):
            d = trk.get_state()  # tlbr
            if (trk.time_since_update < 1) and (trk.hit_streak >= self.min_hits or self.frame_count <= self.min_hits):
                ret.append(
                    np.concatenate((d, [trk.track_id + 1])).reshape(1, -1))  # +1 as MOT benchmark requires positive
            i -= 1
            # remove dead track let
            if trk.time_since_update > self.max_age:
                self.tracks.pop(i)
        if len(ret) > 0:
            return np.concatenate(ret)
        return np.empty((0, 5))
