import numpy as np

from src.constant.correlation.staple_config import StapleConfig
from src.tracking.modules.selector.local_based.dat import DAT
from src.tracking.modules.selector.local_based.mosse import MOSSE
from src.tracking.modules.selector.local_based.staple import Staple


class VisualTracker(object):
    def __init__(self, visual_type):
        self.visual_type = visual_type
        self.tracks = []

    def init(self, frame, boxes):
        self.tracks = []
        for _ in boxes:
            if self.visual_type == 'Mosse':
                self.tracks.append(MOSSE())
            elif self.visual_type == 'Staple':
                self.tracks.append(Staple(config=StapleConfig()))
            elif self.visual_type == 'Dat':
                self.tracks.append(DAT())

        for idx, track_tmp in enumerate(self.tracks):
            track_tmp.init(frame, boxes[idx])

        # assert len(self.tracks) == boxes.shape[0]

    def update(self, frame):
        boxes = []
        for track in self.tracks:
            boxes.append(track.update(frame))
        return np.array(boxes)
