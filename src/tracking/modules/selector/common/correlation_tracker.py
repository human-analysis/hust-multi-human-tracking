# from dlib import correlation_tracker, rectangle
# from numpy import long
#
#
# class CorrelationTracker:
#     count = 0
#
#     def __init__(self, bbox, img, max_lost_time=5):
#         self.tracker = correlation_tracker()
#         self.tracker.start_track(img, rectangle(long(bbox[0]), long(bbox[1]), long(bbox[2]), long(bbox[3])))
#         self.confidence = 0.  # measures how confident the tracker is! (a.k.a. correlation score)
#
#         self.time_since_update = 0
#         self.id = CorrelationTracker.count
#         CorrelationTracker.count += 1
#         self.hits = 0
#         self.hit_streak = 0
#         self.age = 0
#         self.centroids = []
#         self.confirmed = True
#         self.max_lost_time = max_lost_time
#
#     def predict(self, img):
#         self.confidence = self.tracker.update(img)
#
#         self.age += 1
#         if self.time_since_update > self.max_lost_time:  # > 0
#             self.hit_streak = 0
#         self.time_since_update += 1
#
#         bbox = self.get_state()[0]
#         self.centroids.append([(bbox[0] + bbox[2]) / 2, (bbox[1] + bbox[3]) / 2])
#         while len(self.centroids) >= 10:
#             self.centroids.pop(0)
#
#         return bbox
#
#     def is_confirmed(self):
#         return self.confirmed
#
#     def update(self, bbox, img):
#         self.time_since_update = 0
#         self.hits += 1
#         self.hit_streak += 1
#
#         '''re-start the tracker with detected positions (it detector was active)'''
#         if bbox != []:
#             self.tracker.start_track(img, rectangle(long(bbox[0]), long(bbox[1]), long(bbox[2]), long(bbox[3])))
#         '''
#         Note: another approach is to re-start the tracker only when the correlation score fall below some threshold
#         i.e.: if bbox !=[] and self.confidence < 10.
#         but this will reduce the algo. ability to track objects through longer periods of occlusions.
#         '''
#
#     def get_state(self):
#         pos = self.tracker.get_position()
#         return [pos.left(), pos.top(), pos.right(), pos.bottom()]
