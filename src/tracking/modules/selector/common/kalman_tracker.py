import numpy as np
from filterpy.kalman import KalmanFilter

from src.tracking.modules.utils import convert_bbox_to_z, convert_x_to_bbox

KalmanFilter.compute_log_likelihood = False


class KalmanBoxTracker(object):
    """
    This class represents the internel state of individual tracked objects observed as bbox.
    """
    count = 0

    def __init__(self, bbox, image=None, max_lost_time=5):
        """
        bbox xyxy
        Initialises a tracker using initial bounding box.
        """
        # define constant velocity model
        self.kf = KalmanFilter(dim_x=7, dim_z=4)
        self.kf.F = np.array(
            [[1, 0, 0, 0, 1, 0, 0], [0, 1, 0, 0, 0, 1, 0], [0, 0, 1, 0, 0, 0, 1], [0, 0, 0, 1, 0, 0, 0],
             [0, 0, 0, 0, 1, 0, 0], [0, 0, 0, 0, 0, 1, 0], [0, 0, 0, 0, 0, 0, 1]])
        self.kf.H = np.array(
            [[1, 0, 0, 0, 0, 0, 0], [0, 1, 0, 0, 0, 0, 0], [0, 0, 1, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0]])

        self.kf.R[2:, 2:] *= 10.
        self.kf.P[4:, 4:] *= 1000.  # give high uncertainty to the unobservable initial velocities
        self.kf.P *= 10.
        self.kf.Q[-1, -1] *= 0.01 # 0.01
        self.kf.Q[4:, 4:] *= 0.01 # 0.01

        self.kf.x[:4] = convert_bbox_to_z(bbox)
        self.time_since_update = 0
        self.track_id = KalmanBoxTracker.count
        KalmanBoxTracker.count += 1
        self.history = []
        self.centroids = []
        self.hits = 0
        self.hit_streak = 0
        self.age = 0
        self.waiting_frames = 0
        self.confirmed = True
        self.max_lost_time = max_lost_time

    def update(self, bbox):
        """
        bbox: tlbr
        Updates the state vector with observed bbox.
        """
        self.time_since_update = 0
        self.history = []
        self.hits += 1
        self.hit_streak += 1
        self.kf.update(convert_bbox_to_z(bbox))

    def is_confirmed(self):
        return self.confirmed

    def predict(self):
        """
        Advances the state vector and returns the predicted bounding box estimate.
        """
        if (self.kf.x[6] + self.kf.x[2]) <= 0:
            self.kf.x[6] *= 0.0
        self.kf.predict()
        self.age += 1

        if self.time_since_update > 0:  # self.max_lost_time
            self.hit_streak = 0
        self.time_since_update += 1

        bbox = convert_x_to_bbox(self.kf.x)[0]

        self.centroids.append([(bbox[0] + bbox[2]) / 2, (bbox[1] + bbox[3]) / 2])
        while len(self.centroids) >= 20:
            self.centroids.pop(0)

        # self.history.append(bbox)
        # if len(self.history) >= 50:
        #     self.history.pop(0)

        return bbox

    def get_centroids(self):
        return self.centroids

    def to_tlbr(self):
        return convert_x_to_bbox(self.kf.x)[0]

    def get_state(self):
        """
        Returns the current bounding box estimate.
        """
        # bbox = convert_x_to_bbox(self.kf.x)[0]

        return convert_x_to_bbox(self.kf.x)[0]
