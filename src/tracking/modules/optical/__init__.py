#! /usr/bin/env python
# -*- coding: utf-8 -*-
import cv2
import numpy as np

from .extraction import getFeatures
from .geometric_transformation import applyGeometricTransformation
from .translation_estimation import estimateAllTranslation


def optical_flow_tracking(rawVideo, draw_bb=False, play_realtime=False, save_to_file=False):
    """
    Description: 
        This function implements opencv optical flow for each ROI
        Given oldframe, newframe and old corners, we can get new corners and update accordingly
        Usage: optical_bbox = optical_flow(bbox, oldframe, newframe)
        :param rawVideo: 
        :param draw_bb: 
        :param play_realtime: 
        :param save_to_file: 
        :return: 
    """
    # initialize
    n_frame = 400
    frames = np.empty((n_frame,), dtype=np.ndarray)
    frames_draw = np.empty((n_frame,), dtype=np.ndarray)
    boxes = np.empty((n_frame,), dtype=np.ndarray)
    for frame_idx in range(n_frame):
        _, frames[frame_idx] = rawVideo.read()

    showCrosshair = False
    fromCenter = False
    # draw rectangle roi for target objects, or use default objects initilization
    if draw_bb:
        n_object = int(input("Number of objects to selector:"))
        boxes[0] = np.empty((n_object, 4, 2), dtype=float)
        for i in range(n_object):
            (x_min, y_min, box_w, box_h) = cv2.selectROI("Select Object %d" % (i), frames[0], fromCenter, showCrosshair)
            cv2.destroyWindow("Select Object %d" % (i))
            boxes[0][i, :, :] = np.array(
                [[x_min, y_min], [x_min + box_w, y_min], [x_min, y_min + box_h],
                 [x_min + box_w, y_min + box_h]]).astype(float)
    else:
        n_object = 1
        boxes[0] = np.array([[[291, 187], [405, 187], [291, 267], [405, 267]]]).astype(float)

    if save_to_file:
        out = cv2.VideoWriter('detection.avi', 0, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), 20.0,
                              (frames[i].shape[1], frames[i].shape[0]))

    # Start from the first frame, do optical flow for every two consecutive frames.
    startXs, startYs = getFeatures(cv2.cvtColor(frames[0], cv2.COLOR_RGB2GRAY), boxes[0], use_shi=False)
    for i in range(1, n_frame):
        print('Processing Frame', i)
        newXs, newYs = estimateAllTranslation(startXs, startYs, frames[i - 1], frames[i])
        Xs, Ys, boxes[i] = applyGeometricTransformation(startXs, startYs, newXs, newYs, boxes[i - 1])

        # update coordinates
        startXs = Xs
        startYs = Ys

        # update feature points as required
        n_features_left = np.sum(Xs != -1)
        print('# of Features: %d' % n_features_left)
        if n_features_left < 15:
            print('Generate New Features')
            startXs, startYs = getFeatures(cv2.cvtColor(frames[i], cv2.COLOR_RGB2GRAY), boxes[i])

        # draw bounding box and visualize feature point for each object
        frames_draw[i] = frames[i].copy()
        for j in range(n_object):
            (x_min, y_min, box_w, box_h) = cv2.boundingRect(boxes[i][j, :, :].astype(int))
            frames_draw[i] = cv2.rectangle(frames_draw[i], (x_min, y_min), (x_min + box_w, y_min + box_h), (255, 0, 0),
                                           2)
            # red color features
            for k in range(startXs.shape[0]):
                frames_draw[i] = cv2.circle(frames_draw[i], (int(startXs[k, j]), int(startYs[k, j])), 3, (0, 0, 255),
                                            thickness=2)

        # imshow if to play the result in real time
        if play_realtime:
            cv2.imshow("win", frames_draw[i])
            cv2.waitKey(10)
        if save_to_file:
            out.write(frames_draw[i])

    if save_to_file:
        out.release()


def bbox_transform(new_boxes, boxes):
    for i in range(new_boxes.shape[0]):
        if not np.isnan(np.sum(new_boxes)):
            boxes[i] = [new_boxes[i, 0, 0], new_boxes[i, 0, 1], new_boxes[i, 3, 0] - new_boxes[i, 0, 0],
                        new_boxes[i, 3, 1] - new_boxes[i, 0, 1]]
    return boxes
