'''
  File name: translation_estimation.py
  Author: 
  Date created: 2018-11-05
'''

import cv2
import numpy as np

from .feature_estimation import estimateFeatureTranslation


def estimateAllTranslation(start_xs, start_ys, img1, img2):
    I = cv2.cvtColor(img1, cv2.COLOR_RGB2GRAY)
    I = cv2.GaussianBlur(I, (5, 5), 0.2)
    Iy, Ix = np.gradient(I.astype(float))

    startXs_flat = start_xs.flatten()
    startYs_flat = start_ys.flatten()
    newXs = np.full(startXs_flat.shape, -1, dtype=float)
    newYs = np.full(startYs_flat.shape, -1, dtype=float)

    for i in range(np.size(start_xs)):
        if startXs_flat[i] != -1:
            newXs[i], newYs[i] = estimateFeatureTranslation(startXs_flat[i], startYs_flat[i], Ix, Iy, img1, img2)

    newXs = np.reshape(newXs, start_xs.shape)
    newYs = np.reshape(newYs, start_ys.shape)
    return newXs, newYs
