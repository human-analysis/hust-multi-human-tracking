import numpy as np
from skimage import transform as tf


def applyGeometricTransformation(start_xs, start_ys, new_xs, new_ys, bbox):
    n_object = bbox.shape[0]
    new_bbox = np.zeros_like(bbox)
    Xs = new_xs.copy()
    Ys = new_ys.copy()
    for obj_idx in range(n_object):
        startXs_obj = start_xs[:, [obj_idx]]
        startYs_obj = start_ys[:, [obj_idx]]
        newXs_obj = new_xs[:, [obj_idx]]
        newYs_obj = new_ys[:, [obj_idx]]
        desired_points = np.hstack((startXs_obj, startYs_obj))
        actual_points = np.hstack((newXs_obj, newYs_obj))
        t = tf.SimilarityTransform()
        t.estimate(dst=actual_points, src=desired_points)
        mat = t.params

        # estimate the new bounding box with all the feature points
        # coords = np.vstack((bbox[obj_idx,:,:].T,np.array([1,1,1,1])))
        # new_coords = mat.dot(coords)
        # new_bbox[obj_idx,:,:] = new_coords[0:2,:].T

        # estimate the new bounding box with only the inliners (Added by Yongyi Wang)
        THRESHOLD = 1
        projected = mat.dot(np.vstack((desired_points.T.astype(float), np.ones([1, np.shape(desired_points)[0]]))))
        distance = np.square(projected[0:2, :].T - actual_points).sum(axis=1)
        actual_indies = actual_points[distance < THRESHOLD]
        desired_indies = desired_points[distance < THRESHOLD]
        if np.shape(desired_indies)[0] < 4:
            print('too few points: ', np.shape(desired_indies)[0])
            actual_indies = actual_points
            desired_indies = desired_points
        t.estimate(dst=actual_indies, src=desired_indies)
        mat = t.params
        coords = np.vstack((bbox[obj_idx, :, :].T, np.array([1, 1, 1, 1])))
        new_coords = mat.dot(coords)
        new_bbox[obj_idx, :, :] = new_coords[0:2, :].T
        Xs[distance >= THRESHOLD, obj_idx] = -1
        Ys[distance >= THRESHOLD, obj_idx] = -1

    return Xs, Ys, new_bbox
