from abc import ABC

import numpy as np

from src import system
from src.tracking.modules.selector.box_based.tracker import SortTracker
from src.tracking.templates import TrackerTemplate, OpticalSortTemplate, CorrFilterSortTemplate


@system.TRACKER.register("Sort")
class Sort(TrackerTemplate, ABC):
    def __init__(self, cfg, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.cfg = cfg
        self.tracker = SortTracker(eval(cfg['max_age']), eval(cfg['min_hits']), eval(cfg['use_facial_model']))

    def update(self, boxes, scores, image, *args, **kwargs):
        """
        :param boxes: xywh
        :param scores:
        :param image:
        :param args:
        :param kwargs:
        :return:
        """
        # convert to xyxy
        boxes[:, 2:] = boxes[:, 2:] + boxes[:, :2]
        detections = np.empty((boxes.shape[0], 5))
        detections[:, :4] = boxes
        detections[:, 4] = scores
        detections_res = self.tracker.update(detections, image)

        return detections_res, self.tracker.tracks


@system.TRACKER.register("Optical_Sort")
class OpticalSort(OpticalSortTemplate, ABC):
    def __init__(self, cfg, *args, **kwargs):
        super().__init__(cfg, *args, **kwargs)
        self.main_tracker = Sort(cfg, *args, **kwargs)

    def update(self, boxes, scores, image, *args, **kwargs):
        """
        :param boxes: xywh
        :param scores:
        :param image:
        :param args:
        :param kwargs:
        :return:
        """
        boxes = self._directly_predict(image, boxes)
        return self.main_tracker.update(boxes, scores, image, *args, **kwargs)


@system.TRACKER.register("Corr_Sort")
class CorrSort(CorrFilterSortTemplate, ABC):
    def __init__(self, cfg, *args, **kwargs):
        super().__init__(cfg, *args, **kwargs)
        self.main_tracker = Sort(cfg, *args, **kwargs)

    def update(self, boxes, scores, image, *args, **kwargs):
        """
        :param boxes: xywh
        :param scores:
        :param image:
        :param args:
        :param kwargs:
        :return:
        """
        boxes = self._directly_predict(image, boxes)
        return self.main_tracker.update(boxes, scores, image, *args, **kwargs)
