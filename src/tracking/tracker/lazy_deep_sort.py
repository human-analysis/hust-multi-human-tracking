import os
from abc import ABC

from src import system
from src.tracking.modules.extraction.deep import create_box_encoder
from src.tracking.modules.matching import nn_matching
from src.tracking.modules.selector.deep_based.detection import Detection
from src.tracking.modules.selector.deep_based.tracker import DeepTracker
from src.tracking.templates import TrackerTemplate, OpticalSortTemplate, CorrFilterSortTemplate
from src.utils.box_utils import iou_xywh


@system.TRACKER.register("LazyDeepSort")
class LazyDeepSort(TrackerTemplate, ABC):
    def __init__(self, cfg, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.root = cfg['root']
        self.encoder = create_box_encoder(
            os.path.join(self.root, cfg['reid_checkpoint']),
            batch_size=4096)
        metric = nn_matching.NearestNeighborDistanceMetric("cosine", float(cfg['max_cosine_distance']),
                                                           int(cfg['nn_budget']))
        self.tracker = DeepTracker(metric, max_iou_distance=float(cfg['nms_max_overlap']),
                                   max_age=int(cfg['max_age']), n_init=3)  # 0.7
        self.feature_triples = []
        self.min_consider_iou = 0.4
        self.nms_max_overlap = 0.35  # float(cfg['nms_max_overlap']) >= 0.35 ok
        self.frame_counter = 0
        self.extraction_cycle = 5

    def _get_needed_indices(self, boxes):
        n_boxes = boxes.shape[0]
        box_indices = set()

        for i in range(n_boxes - 1):
            for j in range(i + 1, n_boxes):
                iou_value = iou_xywh(boxes[i], boxes[j])

                if iou_value >= self.min_consider_iou:
                    box_indices.add(i)
                    box_indices.add(j)
        return box_indices

    def _get_nearby_feat(self, box):
        """
        # c1: linear O(n), c2: binary O(log n)
        # remaining boxes user previous boxes -> may down performance (use prediction by Sort for them)
        :param box:
        :return:
        """
        max_iou, index = -1, -1
        for i, feature_pair in enumerate(self.feature_triples):
            iou_value = iou_xywh(box, feature_pair[0])
            if iou_value > self.nms_max_overlap:
                if iou_value > max_iou:
                    max_iou = iou_value
                    index = i

        return index

    def update(self, boxes, scores, image, *args, **kwargs):
        """
        easy strategy: apply gen for overlapping boxes
        advanced strategy: use sort tracker before deep
        :param boxes:
        :param scores:
        :param image:
        :param args:
        :param kwargs:
        :return:
        """
        # boxes[:, 2:] = boxes[:, 2:] - boxes[:, :2]

        # easy strategy
        if self.frame_counter % self.extraction_cycle == 0 or kwargs["is_update"]:
            features = self.encoder(image, boxes)
            self.feature_triples = [[bbox, score, feat] for bbox, score, feat in zip(boxes, scores, features)]
            detections = [Detection(bbox, score, feature) for bbox, score, feature in zip(boxes, scores, features)]
        else:
            n_boxes = boxes.shape[0]
            feature_triples_tmp = []
            needed_box_indices = self._get_needed_indices(boxes)

            remaining_indices = set(range(n_boxes)).difference(needed_box_indices)
            needed_boxes = []
            for index in needed_box_indices:
                needed_boxes.append(boxes[index])

            for box_index in remaining_indices:
                pair_index = self._get_nearby_feat(boxes[box_index])
                if pair_index == -1:
                    needed_box_indices.add(box_index)
                    needed_boxes.append(boxes[box_index])
                else:
                    feature_triples_tmp.append(
                        [boxes[box_index], scores[box_index], self.feature_triples[pair_index][2]])

            features = self.encoder(image, needed_boxes)
            for index, feat in zip(needed_box_indices, features):
                feature_triples_tmp.append([boxes[index], scores[index], feat])

            self.feature_triples = feature_triples_tmp
            assert len(self.feature_triples) == len(boxes)
            detections = [Detection(item[0], item[1], item[2]) for item in self.feature_triples]

        #####
        # update tracker
        self.tracker.predict()
        self.tracker.update(detections)
        self.frame_counter += 1

        detections_res = []
        for track in self.tracker.tracks:
            if not track.is_confirmed() or track.time_since_update > 1:
                continue
            bbox = track.to_tlbr()
            detections_res.append([bbox[0], bbox[1], bbox[2], bbox[3], track.track_id])

        return detections_res, self.tracker.tracks


@system.TRACKER.register("Optical_LazyDeepSort")
class OpticalLazyDeepSort(OpticalSortTemplate, ABC):
    def __init__(self, cfg, *args, **kwargs):
        super().__init__(cfg, *args, **kwargs)
        self.main_tracker = LazyDeepSort(cfg, *args, **kwargs)

    def update(self, boxes, scores, image, *args, **kwargs):
        boxes = self._directly_predict(image, boxes)
        return self.main_tracker.update(image, boxes, scores, *args, **kwargs)


@system.TRACKER.register("Corr_LazyDeepSort")
class CorrLazyDeepSort(CorrFilterSortTemplate, ABC):
    def __init__(self, cfg, *args, **kwargs):
        super().__init__(cfg, *args, **kwargs)
        self.main_tracker = LazyDeepSort(cfg, *args, **kwargs)

    def update(self, boxes, scores, image, *args, **kwargs):
        boxes = self._directly_predict(image, boxes)
        return self.main_tracker.update(image, boxes, scores, *args, **kwargs)
