import os
from abc import ABC

from src import system
from src.tracking.modules.extraction.deep import create_box_encoder
from src.tracking.modules.matching import nn_matching
from src.tracking.modules.selector.deep_based.detection import Detection
from src.tracking.modules.selector.deep_based.tracker import DeepTracker
from src.tracking.templates import TrackerTemplate, OpticalSortTemplate, CorrFilterSortTemplate


@system.TRACKER.register("DeepSort")
class DeepSort(TrackerTemplate, ABC):
    def __init__(self, cfg, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.root = cfg["root"]
        self.encoder = create_box_encoder(
            os.path.join(self.root, cfg['reid_checkpoint']),
            batch_size=4096)
        metric = nn_matching.NearestNeighborDistanceMetric("cosine", float(cfg['max_cosine_distance']),
                                                           int(cfg['nn_budget']))
        self.tracker = DeepTracker(metric, max_iou_distance=float(cfg['nms_max_overlap']),
                                   max_age=int(cfg['max_age']), n_init=3)
        self.feature_tmp = []

    def update(self, boxes, scores, image, *args, **kwargs):
        # features = self.encoder(image, boxes)
        # detections = [Detection(bbox, score, feature) for bbox, score, feature in zip(boxes, scores, features)]

        ###### xywh
        if kwargs['update_fg'] or not (len(boxes) == len(self.feature_tmp)):
            features = self.encoder(image, boxes)
            self.feature_tmp = features
        else:
            features = self.feature_tmp
        # may be add min-confidence filter
        detections = [Detection(bbox, score, feature) for bbox, score, feature in zip(boxes, scores, features)]
        ######

        # nms
        # boxes = np.array([d.tlwh for d in detections])
        # scores = np.array([d.confidence for d in detections])
        # indices = non_max_suppression(boxes, self.nms_max_overlap, scores)
        # detections = [detections[i] for i in indices]

        # update tracker
        self.tracker.predict()
        self.tracker.update(detections)

        detections_res = []
        for track in self.tracker.tracks:
            if not track.is_confirmed() or track.time_since_update > 1:
                continue
            bbox = track.to_tlbr()
            detections_res.append([bbox[0], bbox[1], bbox[2], bbox[3], track.track_id])

        return detections_res, self.tracker.tracks


@system.TRACKER.register("Optical_DeepSort")
class OpticalDeepSort(OpticalSortTemplate, ABC):
    def __init__(self, cfg, *args, **kwargs):
        super().__init__(cfg, *args, **kwargs)
        self.main_tracker = DeepSort(cfg, *args, **kwargs)

    def update(self, boxes, scores, image, *args, **kwargs):
        boxes = self._directly_predict(image, boxes)
        return self.main_tracker.update(image, boxes, scores, *args, **kwargs)


@system.TRACKER.register("Corr_DeepSort")
class CorrDeepSort(CorrFilterSortTemplate, ABC):
    def __init__(self, cfg, *args, **kwargs):
        super().__init__(cfg, *args, **kwargs)
        self.main_tracker = DeepSort(cfg, *args, **kwargs)

    def update(self, boxes, scores, image, *args, **kwargs):
        boxes = self._directly_predict(image, boxes)
        return self.main_tracker.update(image, boxes, scores, *args, **kwargs)
