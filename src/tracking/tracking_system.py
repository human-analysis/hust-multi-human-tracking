import os
from configparser import ConfigParser

import cv2
import imutils

from src.constant.visualization import *
from src.detection import get_detector
from src.tracking import get_tracker
from src.tracking.modules.selector.local_based.visual_tracker import VisualTracker
from src.utils.box_utils import norm_box
from src.utils.demo import WebCamVideoStream, VideoStream, FPS, OnlineStream, get_stream

IS_DETECTION_DISPLAY = True
IS_TRACKING_DISPLAY = True
IS_TRACKING_TRACE = True
points = []


class TrackingCreator:
    def __init__(self, detector_name,
                 tracker_name,
                 config_path,
                 auxiliary_tracker_name,
                 type_tracking="offline"):
        self.cfg = ConfigParser()
        self.cfg.read(config_path)

        self.detector_name = detector_name
        self.tracker_name = tracker_name

        self.detector = get_detector(detector_name, self.cfg, "onnx")
        self.tracker = get_tracker(tracker_name, self.cfg)

        if auxiliary_tracker_name in ["Mosse", "Staple", "Dat"]:
            self.auxiliary_tracker = VisualTracker(auxiliary_tracker_name)
        else:
            self.auxiliary_tracker = None

        self.type = type_tracking

    def set_tracker(self, tracker_name):
        self.tracker_name = tracker_name
        self.tracker = get_tracker(tracker_name, self.cfg)

    def set_detector(self, detector_name):
        self.detector_name = detector_name
        self.detector = get_detector(detector_name, self.cfg)

    def track(self, source, output_video, output_file,
              resizing_width=-1, num_frames=-1, confidence=0.3,
              detect_freq=3):

        stream = get_stream(source, self.type)

        frame = stream.frame
        h, w = frame.shape[:2]

        if resizing_width != -1:
            frame = imutils.resize(frame, width=resizing_width)

        stream.start()

        four_cc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
        video_writer = cv2.VideoWriter(output_video, four_cc, 3, (w, h))

        os.makedirs(os.path.dirname(output_file), exist_ok=True)
        text_writer = open(output_file, "w")

        step_counter = 0
        update_fg, detect_fg = True, True
        boxes, scores, detections, tracks, tracking_res = [], [], [], [], []

        fps = FPS()
        fps.start()

        is_detect = True

        while num_frames == -1 or fps.num_frames < num_frames:
            frame_id, frame = stream.read()

            if frame is None:
                break

            if resizing_width != -1:
                frame = imutils.resize(frame, width=resizing_width)

            # ==== processing =====
            if step_counter % detect_freq == 0 or is_detect:
                boxes, scores = self.detector.detect_image(frame.copy(), confidence)

                if boxes.shape[0] > 0:
                    is_detect = False
                    boxes[:, 2:] = boxes[:, 2:] - boxes[:, :2]
                    self.tracker.set_detection_needed(False)


            if boxes.shape[0] > 0:
                # convert to xywh
                # wrong when skip
                detections, tracks = self.tracker.update(boxes.copy(),
                                                         scores.copy(),
                                                         frame.copy(),
                                                         update_fg=update_fg)

            step_counter += 1
            fps.update()
            fps.stop()

            cv2.putText(frame, 'FPS:' + str(round(fps.fps(), 1)), (0, 25),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

            ####
            if boxes.shape[0] == 0:
                video_writer.write(frame)
                continue
            ####

            if self.type == "offline":
                frame_id = fps.num_frames

            if IS_TRACKING_DISPLAY:
                for track in tracks:
                    if not track.is_confirmed() or track.time_since_update > 5:  # > 1
                        continue

                    if IS_TRACKING_TRACE:
                        track_centroids = track.get_centroids()
                        for i in range(0, len(track_centroids) - 1):
                            # Draw trace line
                            x1, y1 = track_centroids[i][0], track_centroids[i][1]
                            x2, y2 = track_centroids[i + 1][0], track_centroids[i + 1][1]
                            clr = track.track_id % 9
                            cv2.line(frame, (int(x1), int(y1)), (int(x2), int(y2)),
                                     track_colors[clr], 2)

            if IS_DETECTION_DISPLAY:
                for i, detection in enumerate(detections):
                    alpha = 0.3
                    frame_copy = frame.copy()
                    detection = norm_box(detection)

                    tracking_res.append([frame_id, detection[4],
                                         detection[0], detection[1],
                                         detection[2] - detection[0],
                                         detection[3] - detection[1]])

                    cv2.rectangle(frame, (int(detection[0]), int(detection[1])),
                                  (int(detection[2]), int(detection[3])), colors[18], -1)
                    cv2.addWeighted(frame, alpha, frame_copy, 1 - alpha, 0, frame_copy)

                    frame = frame_copy.copy()
                    cv2.rectangle(frame, (int(detection[0]), int(detection[1])),
                                  (int(detection[2]), int(detection[3])), colors[18], 1)
                    cv2.putText(frame, f"{detection[4]}",
                                (int(detection[0]), int(detection[1])), cv2.FONT_ITALIC, 0.3,
                                (31, 20, 245), 1)

            video_writer.write(frame)
            # ==== end process ====

        fps.stop()

        for line in tracking_res:
            print(f"{int(line[0])},{int(line[1])},{int(line[2])},{int(line[3])},"
                  f"{int(line[4])},{int(line[5])},-1,-1,-1,-1",
                  file=text_writer)

        text_writer.close()

        print("Elapsed time : {:.2f}".format(fps.elapsed()))
        print("Approx. FPS: {:.2f}".format(fps.fps()))

        stream.stop()
        video_writer.release()
        cv2.destroyAllWindows()

    def track_with_aux_tracker(self, source, output_video, output_file,
                               resizing_width=-1, num_frames=-1,
                               detect_freq=3):

        if self.type == "offline":
            if type(source) != str:
                stream = WebCamVideoStream(src=int(source))
            else:
                stream = VideoStream(source)
        else:
            stream = OnlineStream(source)

        frame = stream.frame
        h, w = frame.shape[:2]

        if resizing_width != -1:
            frame = imutils.resize(frame, width=resizing_width)

        stream.start()

        four_cc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
        video_writer = cv2.VideoWriter(output_video, four_cc, 3, (w, h))

        os.makedirs(os.path.dirname(output_file), exist_ok=True)
        text_writer = open(output_file, "w")

        step_counter = 0
        update_fg, detect_fg = True, True
        boxes, scores, detections, tracks, tracking_res = [], [], [], [], []

        fps = FPS()
        fps.start()

        while num_frames == -1 or fps.num_frames < num_frames:
            frame_id, frame = stream.read()

            if frame is None:
                break

            if resizing_width != -1:
                frame = imutils.resize(frame, width=resizing_width)

            # ==== processing =====

            if step_counter % detect_freq == 0 or detect_fg:
                # xyxy
                boxes, scores = self.detector.detect_image(frame.copy())
                self.tracker.set_detection_needed(False)

                if boxes.shape[0] > 0:
                    # conver to xywh
                    boxes[:, 2:] = boxes[:, 2:] - boxes[:, :2]
                    ####
                    update_fg = True
                    self.auxiliary_tracker.init(frame.copy(), boxes.copy())  # xywh
                    detect_fg = False
                    ####

            #####
            else:
                if boxes.shape[0] > 0:
                    boxes = self.auxiliary_tracker.update(frame)  # xywh
                    update_fg = False
                    detect_fg = False
                else:
                    detect_fg = True
            #####

            if boxes.shape[0] > 0:
                # boxes xywh
                detections, tracks = self.tracker.update(boxes.copy(),
                                                         scores,
                                                         frame.copy(),
                                                         update_fg=update_fg)
            step_counter += 1
            fps.update()
            fps.stop()

            cv2.putText(frame, 'FPS:' + str(round(fps.fps(), 1)), (0, 25),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

            ####
            if boxes.shape[0] == 0:
                video_writer.write(frame)
                continue
            ####

            if self.type == "offline":
                frame_id = fps.num_frames

            if IS_TRACKING_DISPLAY:
                for track in tracks:
                    if not track.is_confirmed() or track.time_since_update > 5:  # > 1
                        continue

                    if IS_TRACKING_TRACE:
                        track_centroids = track.get_centroids()
                        for i in range(0, len(track_centroids) - 1):
                            # Draw trace line
                            x1, y1 = track_centroids[i][0], track_centroids[i][1]
                            x2, y2 = track_centroids[i + 1][0], track_centroids[i + 1][1]
                            clr = track.track_id % 9
                            cv2.line(frame, (int(x1), int(y1)), (int(x2), int(y2)),
                                     track_colors[clr], 2)

            if IS_DETECTION_DISPLAY:
                for detection in detections:
                    alpha = 0.3
                    frame_copy = frame.copy()
                    detection = norm_box(detection)

                    tracking_res.append([frame_id, detection[4],
                                         detection[0], detection[1],
                                         detection[2] - detection[0],
                                         detection[3] - detection[1]])

                    cv2.rectangle(frame, (int(detection[0]), int(detection[1])),
                                  (int(detection[2]), int(detection[3])), colors[18], -1)
                    cv2.addWeighted(frame, alpha, frame_copy, 1 - alpha, 0, frame_copy)

                    frame = frame_copy.copy()
                    cv2.rectangle(frame, (int(detection[0]), int(detection[1])),
                                  (int(detection[2]), int(detection[3])), colors[18], 1)

                    cv2.putText(frame, str(detection[4]),
                                (int(detection[0]), int(detection[1])), 0, 5e-3 * 80,
                                (0, 255, 0), 2)

            video_writer.write(frame)
            # ==== end process ====

        fps.stop()

        for line in tracking_res:
            print(f"{int(line[0])},{int(line[1])},{int(line[2])},{int(line[3])},"
                  f"{int(line[4])},{int(line[5])},-1,-1,-1,-1",
                  file=text_writer)

        text_writer.close()

        print("Elapsed time : {:.2f}".format(fps.elapsed()))
        print("Approx. FPS: {:.2f}".format(fps.fps()))
        print("Num frames: {:.2f} - {:.2f}".format(fps.num_frames, step_counter))

        stream.stop()
        video_writer.release()
        cv2.destroyAllWindows()
