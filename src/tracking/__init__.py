from src import system
from .tracker import *


def get_tracker(tracker_type, cfg, *args):
    return system.TRACKER[tracker_type](cfg[tracker_type], *args)
