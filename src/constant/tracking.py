deep_tracker_cfg = {
    "PETS09-S2L1":
        {
            "detectFreq1": {"others": "default", "MOTA": 30.4},
            "detectFreq2": {"others": "default", "MOTA": 21.9},
            "mosse_detFreq1": {}, # the worst tracker -> not use
            "staple_detFreq2": {"others": "default", "MOTA": 21.3}
        },
}

strategy_deep_tracker_cfg = {
    "PETS09-S2L1":
        {
            "detectFreq1": {
                "extraction_cycle_1": {"others": "default",
                                       "min_consider_iou": 0.6,
                                       "nms_max_overlap": 0.35,
                                       "MOTA": 30.4},
                "extraction_cycle_2": {"others": "default",
                                       "min_consider_iou": 0.6,
                                       "nms_max_overlap": 0.35,
                                       "MOTA": 30.8},
                "extraction_cycle_3": {"others": "default",
                                       "min_consider_iou": 0.35,
                                       "nms_max_overlap": 0.4,
                                       "MOTA": 29.7},
            },
            "detectFreq2": {
                "extraction_cycle_1": {"others": "default",
                                       "min_consider_iou": 0.6,
                                       "nms_max_overlap": 0.35,
                                       "MOTA": 21.9},
                "extraction_cycle_2": {"others": "default",
                                       "min_consider_iou": 0.4,
                                       "nms_max_overlap": 0.35,
                                       "MOTA": 21.8}
            },
        },
}
