from src import system
from .detector import *


def get_detector(detector_name, cfg, type_detector="ssd"):
    return system.DETECTOR[type_detector](cfg[detector_name])
