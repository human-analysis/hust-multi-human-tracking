import os
from abc import ABC
from functools import partial

import cv2
import numpy as np
import onnxruntime as ort
from PIL import Image

from src import system
from src.detection.modules.box_head.box_post_processor import filter_box
from src.detection.template import DetectorTemplate
from src.utils.nms import boxes_nms


def build_test_transform(img_size, pixel_mean, image):
    image = cv2.resize(image, (img_size[0], img_size[1]))
    image = image.astype(np.float32)
    image -= np.array(pixel_mean, dtype=np.float32)
    image = np.expand_dims(image, 0)
    image = np.transpose(image, [0, 3, 1, 2])
    return image


@system.DETECTOR.register("onnx")
class OnnxSSDetector(DetectorTemplate, ABC):
    def __init__(self, cfg):
        super().__init__()
        self.root = cfg["root"]
        self.input_key = cfg['onnx_input']

        so = ort.SessionOptions()

        so.graph_optimization_level = ort.GraphOptimizationLevel.ORT_ENABLE_ALL
        self.onnx_base = ort.InferenceSession(
                                os.path.join(self.root, cfg['onnx_checkpoint']), so)

        self.post_processor = partial(filter_box, boxes_nms, eval(cfg['confidence_threshold']),
                                      eval(cfg['nms_threshold']),
                                      eval(cfg['max_objects']))

        self.threshold = eval(cfg['confidence_threshold'])
        self.transform = partial(build_test_transform, eval(cfg['img_size']), eval(cfg['pixel_mean']))

    def _read_image(self, image_file):
        return np.array(Image.open(image_file).convert("RGB"))

    def _preprocess(self, image):
        image = self.transform(image)
        return image

    def detect_image(self, img_input, confidence=0.3, target_size=None, return_image=False, **kwargs):
        if type(img_input) == str:
            image = self._read_image(img_input)
        else:
            image = img_input

        original = image.shape
        image = self._preprocess(image)

        onnx_input = {self.input_key: image}
        scores, boxes = self.onnx_base.run(None, onnx_input)
        scores, boxes = scores[0], boxes[0]
        boxes, scores = self.post_processor(original, boxes, scores)

        # idx = scores > confidence
        # return boxes[idx], scores[idx]
        return boxes, scores
