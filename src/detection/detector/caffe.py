from abc import ABC

import cv2
import numpy as np
from PIL import Image

from src import system
from src.detection.template import DetectorTemplate


@system.DETECTOR.register("caffe")
class CaffeDetector(DetectorTemplate, ABC):
    def __init__(self, cfg, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.net = cv2.dnn.readNetFromCaffe(cfg['root'] + cfg['prototxt'], cfg['root'] + cfg['model'])
        self.image_width = int(cfg['image_width'])
        self.image_height = int(cfg['image_height'])
        self.detect_classes = cfg['detect_classes'].split(',')
        self.ignore_classes = set(cfg['ignore_classes'].split(','))

    def _read_image(self, image_file):
        image = np.array(Image.open(image_file).convert("RGB"))
        return image

    def _preprocess(self, image):
        blob = cv2.dnn.blobFromImage(cv2.resize(image, (self.image_height, self.image_width)), 0.007843,
                                     (self.image_height, self.image_width), (127.5, 127.5, 127.5), False)
        self.net.setInput(blob)

    def detect_image(self, img_input, confidence=0.3, target_size=None, return_image=False, **kwargs):
        if type(img_input) == str:
            image = self._read_image(input)
        else:
            image = img_input

        original = image.shape

        self._preprocess(image)
        detections = self.net.forward()

        boxes = []
        scores = []

        for i in range(detections.shape[2]):
            conf = detections[0, 0, i, 2]
            idx = int(detections[0, 0, i, 1])

            if conf > confidence:
                if self.detect_classes[idx] in self.ignore_classes:
                    continue
                else:
                    box = detections[0, 0, i, 3:7] * np.array([original[1], original[0], original[1], original[0]])
                    boxes.append(box)
                    scores.append(1.0)

        return np.array(boxes), np.array(scores)
