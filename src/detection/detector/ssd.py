import os
from abc import ABC

import numpy as np
import torch
from PIL import Image

from src import system
from src.constant.detection import _C as default_cfg
from src.data import build_transforms
from src.detection.modules.build import build_detection_model
from src.detection.template import DetectorTemplate
from src.utils.checkpoint import CheckPointer

cpu_device = torch.device("cpu")


@system.DETECTOR.register("ssd")
class SingleShortDetector(DetectorTemplate, ABC):
    def __init__(self, cfg):
        super().__init__()
        self.cfg = default_cfg
        self.root = cfg["root"]
        self.cfg.merge_from_file(os.path.join(self.root, cfg["config_file"]))

        self.device = cpu_device
        if torch.cuda.is_available():
            self.device = torch.device(self.cfg.MODEL.DEVICE)
        self.model = build_detection_model(self.cfg)
        self.model = self.model.to(self.device)
        self.model.eval()

        self.transforms = build_transforms(self.cfg, is_train=False)

        checkpointer = CheckPointer(self.model, save_dir=self.cfg.OUTPUT_DIR)
        checkpointer.load(os.path.join(self.root, cfg["original_checkpoint"]),
                          use_latest=cfg["original_checkpoint"] is None)
        weight_file = cfg["original_checkpoint"] if cfg["original_checkpoint"] else checkpointer.get_checkpoint_file()

        print('Loaded weights from {}'.format(weight_file))
        self.score_threshold = eval(cfg["confidence_threshold"])
        self.img_size = [self.cfg.INPUT.IMAGE_SIZE, self.cfg.INPUT.IMAGE_SIZE]

    def _read_image(self, image_file):
        image = np.array(Image.open(image_file).convert("RGB"))
        return image

    def _preprocess(self, image):
        image = self.transforms(image)[0].unsqueeze(0)
        return image

    def _inference(self, image, original_size=None):
        return self.model(image.to(self.device), original_size)

    @torch.no_grad()
    def detect_image(self, input, target_size=None, return_image=False, **kwargs):
        if type(input) == str:
            image = self._read_image(input)
        else:
            image = input

        original = image.shape
        image = self._preprocess(image)

        result = self._inference(image, [original[1], original[0]])[0]
        boxes, scores = result[0], result[1]

        indices = scores > self.score_threshold
        boxes = boxes[indices]
        scores = scores[indices]

        return boxes, scores
