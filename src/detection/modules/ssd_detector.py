from torch import nn

from src.detection.modules.backbone import build_backbone
from src.detection.modules.box_head import build_box_head


class SSDDetector(nn.Module):
    def __init__(self, cfg):
        super().__init__()
        self.cfg = cfg
        self.backbone = build_backbone(cfg)
        self.box_head = build_box_head(cfg)

    def forward(self, images, original_size=None):
        features = self.backbone(images)
        detections = self.box_head(features, original_size)
        return detections
