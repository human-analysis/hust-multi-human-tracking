from src.detection import core
from src.detection.modules.box_head.box_predictor import SSDLiteBoxPredictor, SSDBoxPredictor, \
    FSSDBoxPredictor, FSSDBoxLitePredictor, EfficientBoxPredictor

__all__ = ["make_box_predictor", "SSDLiteBoxPredictor", "SSDBoxPredictor", "FSSDBoxPredictor", "FSSDBoxLitePredictor",
           "EfficientBoxPredictor"]


def make_box_predictor(cfg):
    return core.BOX_PREDICTORS[cfg.MODEL.BOX_PREDICTOR](cfg)
