import numpy as np

from src.utils.nms import custom_nms


def filter_box(nms_fn, confidence_threshold, nms_threshold, max_objects, img_size, batches_boxes, batches_scores):
    processed_boxes = []
    processed_scores = []

    for class_id in range(1, batches_scores.shape[1]):
        scores = batches_scores[:, class_id]
        mask = scores > confidence_threshold

        scores = scores[mask]
        if scores.shape[0] == 0:
            continue

        boxes = batches_boxes[mask, :]

        boxes[:, 0::2] *= img_size[1]
        boxes[:, 1::2] *= img_size[0]

        # keep = nms_fn(boxes, scores, nms_threshold)
        # nmsed_boxes = boxes[keep, :]
        # nmsed_scores = scores[keep]

        nmsed_boxes, nmsed_scores = custom_nms(boxes, scores, nms_threshold)
        keep = nms_fn(nmsed_boxes, nmsed_scores, nms_threshold)
        nmsed_boxes = nmsed_boxes[keep, :]
        nmsed_scores = nmsed_scores[keep]

        # keep, num_to_keep, parent_object_index = nms_fn.gpu_nms(nms_threshold, boxes, scores)
        # keep = boxes_nms(torch.tensor(boxes), torch.tensor(scores), 0.6, 100)

        processed_boxes.append(nmsed_boxes)
        processed_scores.append(nmsed_scores)

    if len(processed_boxes) == 0:
        processed_boxes = np.empty((0, 4))
        processed_scores = np.empty((0, 1))
    else:
        processed_boxes = np.concatenate(processed_boxes, 0)
        processed_scores = np.concatenate(processed_scores, 0)

    if processed_boxes.shape[0] > max_objects:
        keep = (-processed_scores).argsort()[:max_objects]
        processed_scores = processed_scores[keep]
        processed_boxes = processed_boxes[keep, :]

    return processed_boxes, processed_scores
