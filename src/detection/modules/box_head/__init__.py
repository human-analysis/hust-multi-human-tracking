from src.detection import core
from .box_head import NmsBoxHead

__all__ = ['build_box_head', 'NmsBoxHead']


def build_box_head(cfg):
    return core.BOX_HEADS[cfg.MODEL.BOX_HEAD](cfg)
