import torch
import torch.nn.functional as F
from torch import nn

from src.detection import core
from src.detection.modules.anchors.prior_box import PriorBox
from src.detection.modules.box_head.box_predictor_factory import make_box_predictor
from src.detection.modules.box_head.inference import SSDPostProcessor
from src.utils import box_utils


@core.BOX_HEADS.register('NmsBoxHead')
class NmsBoxHead(nn.Module):
    def __init__(self, cfg):
        super().__init__()
        self.cfg = cfg
        self.predictor = make_box_predictor(cfg)
        self.post_processor = SSDPostProcessor(cfg)

        self.priors = PriorBox(self.cfg)()
        if torch.cuda.is_available():
            device = torch.device(cfg.MODEL.DEVICE)
            self.priors = self.priors.to(device)

    def forward(self, features, original_size=None):
        cls_logits, bbox_pred = self.predictor(features)

        scores = F.softmax(cls_logits, dim=2)
        boxes = box_utils.convert_locations_to_boxes(
            bbox_pred, self.priors, self.cfg.MODEL.CENTER_VARIANCE, self.cfg.MODEL.SIZE_VARIANCE
        )

        boxes = box_utils.center_form_to_corner_form(boxes)
        detections = (scores, boxes)
        detections = self.post_processor(detections, original_size)
        return detections
