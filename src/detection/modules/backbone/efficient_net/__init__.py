from src.detection import core
from .efficient_net import EfficientNet
from .efficient_net_advprop import EfficientWrapper
from .efficient_net_lite import effnet_lite0, effnet_lite1, effnet_lite2, effnet_lite3, effnet_lite4

__all__ = ["EfficientNet", "EfficientWrapper", "efficient_net_b0", "efficient_lite_b0", "efficient_lite_b1",
           "efficient_lite_b2",
           "efficient_lite_b3", "efficient_lite_b4"]


@core.BACKBONES.register('efficient_net-b3')
def efficient_net_b3(cfg, pretrained=True):
    if pretrained:
        model = EfficientNet.from_pretrained('efficientnet-b3')
    else:
        model = EfficientNet.from_name('efficientnet-b3')

    return model


@core.BACKBONES.register('efficient_net-b0')
def efficient_net_b0(cfg, pretrained=True):
    if cfg.MODEL.BACKBONE.ADVPROP:
        model = EfficientWrapper('efficientnet-b0', out_channels=cfg.INPUT.IMAGE_SIZE,
                                 config_params=cfg.MODEL.BACKBONE.CFG_PARAMS, pretrained=pretrained,
                                 advprop=True)
    else:
        if pretrained:
            model = EfficientNet.from_pretrained('efficientnet-b0', config_params=cfg.MODEL.BACKBONE.CFG_PARAMS)
        else:
            model = EfficientNet.from_name('efficientnet-b0', config_params=cfg.MODEL.BACKBONE.CFG_PARAMS)

    return model


@core.BACKBONES.register('efficient_lite-b0')
def efficient_lite_b0(cfg, pretrained=True):
    return effnet_lite0(config_params=cfg.MODEL.BACKBONE.CFG_PARAMS)


@core.BACKBONES.register('efficient_lite-b1')
def efficient_lite_b1(cfg, pretrained=True):
    return effnet_lite1(config_params=cfg.MODEL.BACKBONE.CFG_PARAMS)


@core.BACKBONES.register('efficient_lite-b2')
def efficient_lite_b2(cfg, pretrained=True):
    return effnet_lite2(config_params=cfg.MODEL.BACKBONE.CFG_PARAMS)


@core.BACKBONES.register('efficient_lite-b3')
def efficient_lite_b3(cfg, pretrained=True):
    return effnet_lite3(config_params=cfg.MODEL.BACKBONE.CFG_PARAMS)


@core.BACKBONES.register('efficient_lite-b4')
def efficient_lite_b4(cfg, pretrained=True):
    return effnet_lite4(config_params=cfg.MODEL.BACKBONE.CFG_PARAMS)
