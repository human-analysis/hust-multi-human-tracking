from src.detection import core
from src.detection.modules.backbone.efficient_net import efficient_net_b0, efficient_lite_b1, efficient_lite_b0, \
    efficient_lite_b2, efficient_lite_b3, efficient_lite_b4
from src.detection.modules.backbone.mobilenet import MobileNetV2, MobileNetV3
from src.detection.modules.backbone.vgg import vgg16

__all__ = ['build_backbone', 'MobileNetV2', 'MobileNetV3', 'efficient_net_b0', 'efficient_lite_b0', 'efficient_lite_b1',
           'efficient_lite_b2', 'efficient_lite_b3', 'efficient_lite_b4', 'vgg16']


def build_backbone(cfg):
    return core.BACKBONES[cfg.MODEL.BACKBONE.NAME](cfg, cfg.MODEL.BACKBONE.PRETRAINED)
