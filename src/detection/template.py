class DetectorTemplate(object):
    def __init__(self, *args, **kwargs):
        pass

    def _read_image(self, image_file):
        pass

    def _preprocess(self, image):
        pass

    def _detect(self, image):
        pass

    def detect_image(self, img_input, confidence=0.3, target_size=None, return_image=False, **kwargs):
        raise NotImplementedError
