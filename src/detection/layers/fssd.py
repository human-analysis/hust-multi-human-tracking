import torch.nn as nn

# def pyramid_feature_extractor(size):
#     layers = None
#     print(size)
#     if size == 300:
#         layers = [BasicConv(160 * 3, 512, kernel_size=3, stride=1, padding=1),
#                   BasicConv(512, 512, kernel_size=3, stride=2, padding=1),
#                   BasicConv(512, 256, kernel_size=3, stride=2, padding=1),
#                   BasicConv(256, 256, kernel_size=3, stride=2, padding=1),
#                   BasicConv(256, 256, kernel_size=3, stride=1, padding=0),
#                   BasicConv(256, 256, kernel_size=3, stride=1, padding=0)]
#     elif size == 512:
#         layers = [BasicConv(256 * 3, 512, kernel_size=3, stride=1, padding=1),
#                   BasicConv(512, 512, kernel_size=3, stride=2, padding=1),
#                   BasicConv(512, 256, kernel_size=3, stride=2, padding=1),
#                   BasicConv(256, 256, kernel_size=3, stride=2, padding=1),
#                   BasicConv(256, 256, kernel_size=3, stride=2, padding=1),
#                   BasicConv(256, 256, kernel_size=3, stride=2, padding=1),
#                   BasicConv(256, 256, kernel_size=4, padding=1, stride=1)]
#     return layers
from src.detection.layers.base import BasicConv


# from src.modeling.layers.base import BasicConv


def pyramid_feature_extractor(feature_layer, mbox):
    pyramid_feature_layers = []
    feature_transform_channel = int(feature_layer[0][1][-1] / 2)
    # in_channels = len(feature_transform_layers) * feature_transform_channel
    in_channels = 3 * feature_transform_channel
    for layer, depth, box in zip(feature_layer[1][0], feature_layer[1][1], mbox):
        if layer == 'S':
            pyramid_feature_layers += [BasicConv(in_channels, depth, kernel_size=3, stride=2, padding=1)]
            in_channels = depth
        elif layer == '':
            pad = (0, 1)[len(pyramid_feature_layers) == 0]
            pyramid_feature_layers += [BasicConv(in_channels, depth, kernel_size=3, stride=1, padding=pad)]
            in_channels = depth
        else:
            AssertionError('Undefined layer')
    return pyramid_feature_layers


def multibox(fea_channels, cfg, num_classes):
    loc_layers = []
    conf_layers = []
    assert len(fea_channels) == len(cfg)
    for i, fea_channel in enumerate(fea_channels):
        loc_layers += [nn.Conv2d(fea_channel, cfg[i] * 4, kernel_size=3, padding=1)]
        conf_layers += [nn.Conv2d(fea_channel, cfg[i] * num_classes, kernel_size=3, padding=1)]
    return loc_layers, conf_layers


def build_transforms_module(feature_layer):
    transform_layers = []
    feature_transform_channel = int(feature_layer[1][-1] / 2)
    for layer, depth in zip(feature_layer[0], feature_layer[1]):
        transform_layers.append(BasicConv(depth, feature_transform_channel, kernel_size=1, padding=0))
    return transform_layers
