import os
import time
from configparser import ConfigParser
from glob import glob

import click

from src.detection import get_detector
from src.tracking.tracking_system import TrackingCreator
from src.utils.benchmark import benchmark_mAP


@click.command()
@click.option("--constant", required=True, type=str)
@click.option("--detector", required=True)
@click.option("--image_root", required=True)
def benchmark_detection(config, detector, image_root):
    cfg = ConfigParser()
    cfg.read(config)

    image_paths = glob(os.path.join(image_root, "*.jpg"))[:30]
    detector = get_detector(detector, cfg)

    total = 0
    for image_path in image_paths:
        start = time.time()
        boxes, scores = detector.detect_image(image_path)
        total += (time.time() - start)
    print(total / 30)


@click.command()
@click.option("--constant", required=True, type=str)
@click.option("--input_path", required=True, type=str)
@click.option("--output_path", required=True, type=str)
def benchmark_tracking(config, input_path, output_path):
    detectors = ["M2_SSD"]

    trackers = ["Sort", "DeepSort", "LazyDeepSort"]
    visual_trackers = ["", "Staple", "Dat", "Mosse"]

    skip_frames = [1, 2, 3]

    seqs = os.listdir(input_path)
    for detector in detectors:
        for tracker in trackers:
            for visual_tracker in visual_trackers:
                for detect_freq in skip_frames:
                    track_scenario = f"{detector}-{tracker}-{'visual'}-DetFreq{detect_freq}"
                    # fix performance on online tracking why down performace ?
                    demo = TrackingCreator(detector, tracker,
                                           config, visual_tracker)

                    if visual_tracker == "":
                        track_scenario = track_scenario.replace("-visual", "")
                    else:
                        track_scenario.replace("visual", visual_tracker)

                    for seq in seqs:
                        if visual_tracker == "":
                            demo.track(os.path.join(input_path, seq, "img"),
                                       output_video=f"{output_path}/{track_scenario}/{seq}/tracked-video.mp4",
                                       output_file=f"{output_path}/{track_scenario}/{seq}/output.txt",
                                       detect_freq=detect_freq)
                        else:
                            demo.track_with_aux_tracker(os.path.join(input_path, seq, "img"),
                                                        output_video=f"{output_path}/{track_scenario}/{seq}/tracked-video.mp4",
                                                        output_file=f"{output_path}/{track_scenario}/{seq}/output.txt",
                                                        detect_freq=detect_freq)

                    ts_files = os.path.join(output_path, track_scenario)
                    benchmark_mAP(input_path, ts_files, track_scenario, "benchmark/tracking_benchmark.txt")


@click.group()
def cli():
    pass


if __name__ == "__main__":
    cli.add_command(benchmark_detection)
    cli.add_command(benchmark_tracking)
    cli()
