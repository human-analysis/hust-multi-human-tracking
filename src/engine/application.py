from configparser import ConfigParser

import click

from src.detection import get_detector
from src.service import get_service
from src.tracking import get_tracker
from src.tracking.modules.selector.local_based.visual_tracker import VisualTracker
from src.utils.demo import get_stream


@click.command()
@click.option("--source", required=True, type=str)
@click.option("--config", default="config/test.ini", type=str)
@click.option("--detector_name", default="M2_SSD", type=str)
@click.option("--tracker_name", default="Sort", type=str)
@click.option("--aux_name", default="Dat", type=str)
@click.option("--service_name", default="human_counting", type=str)
def run(source, config, detector_name, tracker_name, aux_name, service_name):
    cfg = ConfigParser()
    cfg.read(config)

    stream = get_stream(source, "online")

    detector = get_detector(detector_name, cfg, "onnx")
    tracker = get_tracker(tracker_name, cfg)
    visual_tracker = VisualTracker(aux_name)

    service = get_service(service_name)(detector, tracker, visual_tracker)

    det_freq = 2
    print(f"Running service {service_name}")
    service(stream, f"{detector_name}-{tracker_name}-{aux_name}-DetFreq{det_freq}-{service_name}.mp4", det_freq)


if __name__ == "__main__":
    run()
