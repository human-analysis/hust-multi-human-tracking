import math

import numpy as np

from src.constant.visualization import *
from src.utils.box_utils import norm_box
from src.utils.demo import FPS
from src.utils.geometry import retrieve_inside_boxes, is_inside_polygon, angle_between, is_between_lines
from src.utils.visualization import draw_polygon

n_points_to_check = 10


def is_potential_theft(line1, line2, centroids, centroid):
    # todo
    if len(centroids) == 0:
        return False

    # check between lines
    if not is_between_lines(centroid, line1, line2):
        return False

    first = centroids[max(0, len(centroids) - 10)]
    second = np.mean(centroids)

    angle = angle_between(second - first, centroid - second)
    if angle > math.pi / 2:
        return True
    return False


def paying_warning(detector, tracker, aux_tracker):
    def service(stream, output, detect_freq=3):
        frame = stream.get_frame()
        w, h = frame.shape[:2]

        stream.start()
        four_cc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
        video_writer = cv2.VideoWriter(output, four_cc, 20, (h, w))

        frame_counter = 0
        is_detect, is_update = True, True
        scores = None

        fps = FPS()
        fps.start()

        while True:
            frame_id, frame = stream.read()

            if frame is None:
                break

            # reinitialize
            n_cashiers, n_customers = [0, 0], [0, 0]
            obj_status, obj_warning = [["Absent", "Free"], ["Absent", "Free"]], ["", ""]

            if frame_counter % detect_freq or is_detect:
                boxes, scores = detector.detect_image(frame, confidence=0.1)
                # custom filtering
                tmp_boxes, tmp_scores = [], []

                #####
                for i, box in enumerate(boxes):
                    centroid = [(box[0] + box[2]) / 2, (box[1] + box[3]) / 2]
                    if is_inside_polygon(interest_of_region, centroid):
                        if is_inside_polygon(
                                [[outlier_regions[0], outlier_regions[1]], [outlier_regions[2], outlier_regions[1]],
                                 [outlier_regions[2], outlier_regions[3]], [outlier_regions[0], outlier_regions[3]]],
                                centroid):
                            continue

                        is_box = True
                        for point in outlier_points:
                            if is_inside_polygon(
                                    [[box[0], box[1]], [box[2], box[1]], [box[2], box[3]], [box[0], box[3]]],
                                    point):
                                is_box = False
                        if is_box:
                            tmp_boxes.append(box)
                            tmp_scores.append(scores[i])

                boxes = np.array(tmp_boxes)
                scores = np.array(tmp_scores)
                ######

                if boxes.shape[0] > 0:
                    is_update = True
                    is_detect = False
                    boxes[:, 2:] = boxes[:, 2:] - boxes[:, :2]
                    aux_tracker.init(frame.copy(), boxes.copy())
                else:
                    is_detect = True
            else:
                boxes = aux_tracker.update(frame.copy())
                is_detect = False
                is_update = False

            frame_counter += 1
            if boxes.shape[0] > 0:
                # main flow
                tracking_indices, remaining_indices = retrieve_inside_boxes(customer_regions[1:], boxes)

                required_boxes = boxes[tracking_indices]
                required_scores = scores[tracking_indices]

                detections, tracks = tracker.update(required_boxes.copy(),
                                                    required_scores.copy(),
                                                    frame.copy(),
                                                    is_update=is_update)
                fps.update()
                fps.stop()

                # customer info
                for detection, track in zip(detections, tracks):
                    warning_detection = [False, False]

                    alpha = 0.3
                    frame_copy = frame.copy()
                    detection = norm_box(detection)
                    centroid = np.array([(detection[0] + detection[2]) / 2, (detection[1] + detection[3]) / 2])

                    if is_inside_polygon(customer_regions[1:][0], centroid) == 1:
                        region_id = 0
                    elif is_inside_polygon(customer_regions[1:][1], centroid) == 1:
                        region_id = 1
                    else:
                        region_id = -1

                    # count customer
                    if region_id >= 0:
                        n_customers[region_id] += 1

                        # check leaving warning
                        lines = paying_lines[region_id]

                        if is_inside_polygon(paying_regions[region_id], centroid):
                            track.waiting_frames += 1

                        # check passed line
                        if is_potential_theft(lines[0], lines[1], track.get_centroids(),
                                              centroid) and track.waiting_frames < 20:
                            warning_detection[region_id] = True

                        # update theft warning
                        if warning_detection[0]:
                            obj_warning[0] = "Theft"

                        if warning_detection[1]:
                            obj_warning[1] = "Theft"

                        # show box and id
                        if True in warning_detection:
                            cv2.rectangle(frame, (int(detection[0]), int(detection[1])),
                                          (int(detection[2]), int(detection[3])), colors[10], -1)
                        else:
                            cv2.rectangle(frame, (int(detection[0]), int(detection[1])),
                                          (int(detection[2]), int(detection[3])), colors[18], -1)

                        cv2.addWeighted(frame, alpha, frame_copy, 1 - alpha, 0, frame_copy)
                        frame = frame_copy.copy()
                        cv2.rectangle(frame, (int(detection[0]), int(detection[1])),
                                      (int(detection[2]), int(detection[3])), colors[18], 1)

                        track_centroids = track.get_centroids()
                        for i in range(0, len(track_centroids) - 1):
                            # Draw trace line
                            x1, y1 = track_centroids[i][0], track_centroids[i][1]
                            x2, y2 = track_centroids[i + 1][0], track_centroids[i + 1][1]
                            clr = track.track_id % 9
                            cv2.line(frame, (int(x1), int(y1)), (int(x2), int(y2)),
                                     track_colors[clr], 1)

                # update num customer
                if n_customers[0] > 0:
                    obj_status[0][1] = "Occupied"

                if n_customers[1] > 0:
                    obj_status[1][1] = "Occupied"

                for i, box in enumerate(boxes[remaining_indices]):
                    alpha = 0.3
                    frame_copy = frame.copy()
                    centroid = (box[0] + box[2] / 2, box[1] + box[3] / 2)

                    # count cashiers
                    if is_inside_polygon(counter_regions[0], centroid) == 1:
                        region_id = 0
                    else:
                        region_id = 1

                    # show box and id
                    if region_id < 0:
                        continue

                    n_cashiers[region_id] += 1
                    cv2.rectangle(frame, (int(box[0]), int(box[1])),
                                  (int(box[0] + box[2]), int(box[1] + box[3])), colors[18], -1)
                    cv2.addWeighted(frame, alpha, frame_copy, 1 - alpha, 0, frame_copy)

                    frame = frame_copy.copy()
                    cv2.rectangle(frame, (int(box[0]), int(box[1])),
                                  (int(box[0] + box[2]), int(box[1] + box[3])), colors[18], 1)

                if n_cashiers[0] > 0:
                    obj_status[0][0] = "Present"

                if n_cashiers[1] > 0:
                    obj_status[1][0] = "Present"

            else:
                fps.update()
                fps.stop()

            # default geometry for both
            for poly in counter_regions:
                frame = draw_polygon(frame, poly)

            for poly in customer_regions[1:]:
                frame = draw_polygon(frame, poly)

            # show fps
            text = f'FPS: {round(fps.fps(), 1)}'
            cv2.putText(frame, text,
                        (0, 25), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

            top, left = 600, w - 55
            text_line1 = f"Line 1: Cashier   {n_cashiers[0]}: "
            ret, baseline = cv2.getTextSize(text_line1, fontFace=font, fontScale=0.7, thickness=1)

            cv2.putText(frame, text_line1, (left - 55 - 10 - 10, top + ret[1] + baseline + 17 + 40), font, 0.7,
                        (0, 0, 0), 1,
                        cv2.LINE_AA)

            if obj_status[0][0] == "Present":
                cv2.putText(frame, "Present", (left + 200 - 10 - 10 - 80 + 2, top + ret[1] + baseline + 17 + 40), font,
                            0.7,
                            (0, 0, 0), 1,
                            cv2.LINE_AA)
            else:
                cv2.putText(frame, "Absent", (left + 200 - 10 - 10 - 80 + 2, top + ret[1] + baseline + 17 + 40), font,
                            0.7,
                            (255, 0, 0), 1,
                            cv2.LINE_AA)

            text_line1 = f"        Customer {n_customers[0]}: "
            ret, baseline = cv2.getTextSize(text_line1, fontFace=font, fontScale=0.7, thickness=1)

            cv2.putText(frame, text_line1, (left - 55 - 10 - 10, top + ret[1] + baseline + 30 + 40), font, 0.7,
                        (0, 0, 0), 1,
                        cv2.LINE_AA)

            if obj_warning[0] == "Theft":
                cv2.putText(frame, "Theft", (left + 214 - 10 - 10 - 90, top + ret[1] + baseline + 30 + 40), font, 0.7,
                            (255, 0, 0), 1,
                            cv2.LINE_AA)
            else:
                cv2.putText(frame, obj_status[0][1], (left + 214 - 10 - 10 - 90, top + ret[1] + baseline + 30 + 40),
                            font, 0.7,
                            (0, 0, 0),
                            1,
                            cv2.LINE_AA)

            # line 2 ######################################################
            top, left = 950, 662
            text_line2 = f"Line 2: Cashier   {n_cashiers[0]}: "
            cv2.putText(frame, text_line2,
                        (top + 10 - 10 - 50, left + 10), font, 0.7, (0, 0, 0), 1, cv2.LINE_AA)

            if obj_status[1][0] == "Present":
                cv2.putText(frame, "Present", (top + 240 - 10 + 10 - 66 - 50, left + 10), font, 0.7,
                            (0, 0, 0), 1,
                            cv2.LINE_AA)
            else:
                cv2.putText(frame, "Absent", (top + 240 - 10 + 10 - 68 - 50, left + 10), font, 0.7,
                            (0, 0, 0), 1,
                            cv2.LINE_AA)
            text_line2 = f"        Customer {n_customers[1]}: "
            cv2.putText(frame, text_line2,
                        (top + 10 - 10 - 50, left + 22), font, 0.7, (0, 0, 0), 1, cv2.LINE_AA)

            if obj_warning[1] == "Theft":
                cv2.putText(frame, "Theft", (top + 255 - 10 - 70 - 50, left + 23), font, 0.7,
                            (255, 0, 0), 1,
                            cv2.LINE_AA)
            else:
                cv2.putText(frame, obj_status[1][1], (top + 255 - 10 - 70 - 50, left + 23), font, 0.7,
                            (0, 0, 0),
                            1,
                            cv2.LINE_AA)

            ##
            frame_copy = frame.copy()
            cv2.rectangle(frame, (top - 375, left - 50 + 40),
                          (top - 95, left - 10 + 40), (106, 110, 117), -1)

            cv2.addWeighted(frame, 0.3, frame_copy, 0.7, 0, frame_copy)
            frame = frame_copy.copy()
            cv2.rectangle(frame, (top - 375, left - 50 + 40),
                          (top - 95, left - 10 + 40), (106, 110, 117), 1)

            frame_copy = frame.copy()
            cv2.rectangle(frame, (top - 70, left - 50 + 40),
                          (top - 70 + 280, left - 10 + 40), (106, 110, 117), -1)
            cv2.addWeighted(frame, 0.3, frame_copy, 0.7, 0, frame_copy)
            frame = frame_copy.copy()
            cv2.rectangle(frame, (top - 70, left - 50 + 40),
                          (top - 70 + 280, left - 10 + 40), (106, 110, 117), 1)

            video_writer.write(frame)

        fps.stop()
        video_writer.release()
        stream.stop()
        cv2.destroyAllWindows()

        print("Elapsed time : {:.2f}".format(fps.elapsed()))
        print("Approx. FPS: {:.2f}".format(fps.fps()))
        print("Num frames: {:.2f} - {:.2f}".format(fps.num_frames, frame_counter))

    return service
