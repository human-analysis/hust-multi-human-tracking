from .counting import human_counting
from .monitoring import cashier_monitoring
from .warning import paying_warning


def get_service(service_name):
    return globals().get(service_name)
