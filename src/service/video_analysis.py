import math

import cv2
import numpy as np

from src.constant.visualization import human_counting_region
from src.utils.box_utils import iou
from src.utils.demo import FPS
from src.utils.geometry import poly_area, is_inside_polygon

detect_freq = 2
meter_per_pixel = 0.05


def counting_analysis(stream, detector, tracker, aux_tracker, output):
    frame = stream.get_frame()

    print(f"Region of interest: {human_counting_region}")
    S_ROI = poly_area(np.array(human_counting_region))
    w, h = frame.shape[:2]

    S_ROI = 1000

    stream.start()

    video_fps = 3
    four_cc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
    video_writer = cv2.VideoWriter(output, four_cc, video_fps, (h, w))

    frame_counter = 0
    inners, outers = set(), set()
    update_fg, detect_fg = True, True
    boxes, scores = [], []

    fps = FPS()
    fps.start()

    while True:
        ret, frame = stream.read()
        mask = np.zeros([w, h], dtype=np.float32)

        if frame is None:
            break

        if frame_counter % detect_freq or detect_fg:
            boxes, scores = detector.detect_image(frame, confidence=0.7)

            if boxes.shape[0] > 0:
                # convert to xywh
                boxes[:, 2:] = boxes[:, 2:] - boxes[:, :2]
                update_fg = True
                detect_fg = False
                aux_tracker.init(frame.copy(), boxes.copy())
            else:
                detect_fg = True
        else:
            boxes = aux_tracker.update(frame.copy())
            update_fg = False
            detect_fg = False

        frame_counter += 1

        # visual region
        for i in range(len(human_counting_region)):
            cv2.line(frame, (human_counting_region[i % len(human_counting_region)][0],
                             human_counting_region[i % len(human_counting_region)][1]),
                     (human_counting_region[(i + 1) % len(human_counting_region)][0],
                      human_counting_region[(i + 1) % len(human_counting_region)][1]), (0, 255, 0), 1)

        detections, tracks = tracker.update(boxes.copy(),
                                            scores.copy(),
                                            frame.copy(),
                                            update_fg)
        fps.update()
        fps.stop()

        S_total = 0
        for i, detection in enumerate(detections):
            # calculate density of person
            width, height = detection[2] - detection[0], detection[3] - detection[1]
            centroid = ((detection[0] + detection[2]) / 2, (detection[1] + detection[3]) / 2)

            if is_inside_polygon(human_counting_region, centroid):
                S_total += width * height

        for i, detection in range(len(detections) - 1):
            centroid_i = ((detections[i][0] + detections[i][2]) / 2,
                          (detections[i][1] + detections[i][3]) / 2)
            for j in range(i + 1, len(detections)):
                centroid_j = ((detections[j][0] + detections[j][2]) / 2,
                              (detections[j][1] + detections[j][3]) / 2)
                if is_inside_polygon(human_counting_region, centroid_i) \
                        and is_inside_polygon(human_counting_region, centroid_j):
                    S_total -= iou(detections[i], detections[j])

        density = S_total * 100.0 / S_ROI
        print('Density: ', density)

        for track in tracks:
            # Calculate velocity
            bbox = track.get_bbox()
            track_centroids = track.get_centroids()
            if len(track_centroids) >= 8:
                j = len(track_centroids) - 2
                x1, y1 = track_centroids[j][0], track_centroids[j][1]
                x2, y2 = track_centroids[j + 1][0], track_centroids[j + 1][1]
                d = math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
                v1 = d * (video_fps - 1) * meter_per_pixel  # m/s
                v2 = v1 * 3.6  # km/h

                conf_str = '{:.2f} km/h'.format(v2)
                font = cv2.FONT_HERSHEY_COMPLEX_SMALL

                ret, baseline = cv2.getTextSize(conf_str, fontFace=font, fontScale=0.5, thickness=1)
                cv2.rectangle(frame, (bbox[0], bbox[1]), (bbox[0] + ret[0], bbox[1] + ret[1] + baseline),
                              color=(200, 0, 200), thickness=-1)
                cv2.putText(frame, conf_str, (bbox[0], bbox[1] + ret[1] + baseline), font, 0.5, (0, 0, 0), 1,
                            cv2.LINE_AA)
