import cv2
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm

from src.constant.visualization import track_colors, colors, human_counting_region
from src.utils.box_utils import norm_box, iou
from src.utils.demo import FPS
from src.utils.geometry import is_inside_polygon

mask_score, heat_threshold = 0.01, 0.1


def human_counting(detector, tracker, aux_tracker):
    def service(stream, output, detect_freq=3):
        frame = stream.get_frame()

        print(f"Region of interest: {human_counting_region}")
        w, h = frame.shape[:2]

        S_ROI = 1000

        stream.start()

        four_cc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
        video_writer = cv2.VideoWriter(output, four_cc, 3, (h, w))

        frame_counter = 0
        inners, outers = set(), set()
        update_fg, detect_fg = True, True
        boxes, scores = [], []

        fps = FPS()
        fps.start()

        accumulated_exposures = np.zeros([w, h], dtype=np.float32)
        while True:
            ret, frame = stream.read()
            mask = np.zeros([w, h], dtype=np.float32)

            if frame is None:
                break

            if frame_counter % detect_freq or detect_fg:
                boxes, scores = detector.detect_image(frame, confidence=0.7)

                if boxes.shape[0] > 0:
                    # convert to xywh
                    boxes[:, 2:] = boxes[:, 2:] - boxes[:, :2]
                    update_fg = True
                    detect_fg = False
                    aux_tracker.init(frame.copy(), boxes.copy())
                else:
                    detect_fg = True
            else:
                boxes = aux_tracker.update(frame.copy())
                update_fg = False
                detect_fg = False

            frame_counter += 1

            for i in range(len(human_counting_region)):
                cv2.line(frame, (human_counting_region[i % len(human_counting_region)][0],
                                 human_counting_region[i % len(human_counting_region)][1]),
                         (human_counting_region[(i + 1) % len(human_counting_region)][0],
                          human_counting_region[(i + 1) % len(human_counting_region)][1]), (0, 255, 0), 1)

            if boxes.shape[0] == 0:
                fps.update()
                fps.stop()

                cv2.putText(frame, f'FPS: {round(fps.fps(), 1)} In: {len(inners)} - Out: {len(outers)}',
                            (0, 25), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

                if heat_threshold >= 0:
                    heatmap = np.uint8(cm.get_cmap(plt.get_cmap("Accent"))(accumulated_exposures) * 255)
                    heatmap = cv2.cvtColor(heatmap, cv2.COLOR_RGBA2BGR)
                    frame = cv2.addWeighted(heatmap, 0.2, frame, 0.8, 0, frame)

                video_writer.write(frame)
                continue

            detections, tracks = tracker.update(boxes.copy(),
                                                scores.copy(),
                                                frame.copy(),
                                                update_fg)
            fps.update()
            fps.stop()

            cv2.putText(frame, 'FPS:' + str(round(fps.fps(), 1)), (0, 25),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

            for track in tracks:
                track_centroids = track.get_centroids()
                for i in range(0, len(track_centroids) - 1):
                    x1, y1 = track_centroids[i][0], track_centroids[i][1]
                    x2, y2 = track_centroids[i + 1][0], track_centroids[i + 1][1]
                    clr = track.track_id % 9
                    cv2.line(frame, (int(x1), int(y1)), (int(x2), int(y2)),
                             track_colors[clr], 2)

            for detection in detections:
                alpha = 0.3
                frame_copy = frame.copy()
                detection = norm_box(detection)

                width, height = detection[2] - detection[0], detection[3] - detection[1]
                centroid = ((detection[0] + detection[2]) / 2, (detection[1] + detection[3]) / 2)

                mask[int(centroid[0] - width / 8):int(centroid[0] + width / 8),
                int(centroid[1] - height / 8):int(centroid[1] + height / 8)] += mask_score

                mask[int(centroid[0] - width / 4):int(centroid[0] + width / 4),
                int(centroid[1] - height / 4):int(centroid[1] + height / 4)] += mask_score

                cv2.rectangle(frame, (int(detection[0]), int(detection[1])),
                              (int(detection[2]), int(detection[3])), colors[18], -1)
                cv2.addWeighted(frame, alpha, frame_copy, 1 - alpha, 0, frame_copy)

                frame = frame_copy.copy()
                cv2.rectangle(frame, (int(detection[0]), int(detection[1])),
                              (int(detection[2]), int(detection[3])), colors[18], 1)

                cv2.putText(frame, str(detection[4]),
                            (int(detection[0]), int(detection[1])), 0, 5e-3 * 80,
                            (0, 255, 0), 2)

                if is_inside_polygon(human_counting_region, centroid) > 0:
                    inners.add(round(detection[4]))
                    if round(detection[4]) in outers:
                        outers.remove(round(detection[4]))
                else:
                    outers.add(round(detection[4]))
                    if round(detection[4]) in inners:
                        inners.remove(round(detection[4]))

                # # calculate velocity
                # S_total = 0
                # x, y, u, v = box_detected[i]
                # S_total += (u - x + 1) * (v - y + 1)
                # for i in range(len(box_detected) - 1):
                #     for j in range(i + 1, len(box_detected)):
                #         S_total -= iou(box_detected[i], box_detected[j])
                # density = S_total * 100.0 / S_ROI
                # print('Density: ', density)

            cv2.putText(frame, f'FPS: {round(fps.fps(), 1)} In: {len(inners)} - Out: {len(outers)}',
                        (0, 25), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

            if heat_threshold >= 0:
                accumulated_exposures += mask
                accumulated_exposures[accumulated_exposures <= heat_threshold] = 0

                heatmap = np.uint8(cm.get_cmap(plt.get_cmap("Accent"))(accumulated_exposures) * 255)
                heatmap = cv2.cvtColor(heatmap, cv2.COLOR_RGBA2BGR)

                cv2.addWeighted(frame, 0.2, heatmap, 0.8, 0, frame.copy())

            video_writer.write(frame)

        fps.stop()
        video_writer.release()
        stream.stop()
        cv2.destroyAllWindows()

        print("Elapsed time : {:.2f}".format(fps.elapsed()))
        print("Approx. FPS: {:.2f}".format(fps.fps()))
        print("Num frames: {:.2f} - {:.2f}".format(fps.num_frames, frame_counter))

    return service
