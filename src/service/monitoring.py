import cv2

from src.constant.visualization import *
from src.utils.box_utils import norm_box
from src.utils.demo import FPS
from src.utils.service import analyze_track
from src.utils.visualization import draw_polygon


def cashier_monitoring(detector, tracker, aux_tracker):
    def service(stream, output, detect_freq):

        frame = stream.get_frame()
        w, h = frame.shape[:2]
        stream.start()

        four_cc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
        video_writer = cv2.VideoWriter(output, four_cc, 3, (h, w))

        frame_counter = 0
        update_fg, detect_fg = True, True
        boxes, scores = [], []
        is_in_counter, n_customers = [0] * len(counter_regions), [0] * len(counter_regions)
        #
        fps = FPS()
        fps.start()
        #
        while True:
            ret, frame = stream.read()
            if frame is None:
                break

            if frame_counter % detect_freq or detect_fg:
                boxes, scores = detector.detect_image(frame, confidence=0.7)

                if boxes.shape[0] > 0:
                    # convert to xywh
                    boxes[:, 2:] = boxes[:, 2:] - boxes[:, :2]
                    update_fg = True
                    detect_fg = False
                    aux_tracker.init(frame.copy(), boxes.copy())
                else:
                    detect_fg = True
            else:
                boxes = aux_tracker.update(frame.copy())
                update_fg = False
                detect_fg = False

            frame_counter += 1

            for poly in counter_regions[1:]:
                frame = draw_polygon(frame, poly)

            for poly in customer_regions:
                frame = draw_polygon(frame, poly)

            if boxes.shape[0] == 0:
                fps.update()
                fps.stop()

                for i in range(len(is_in_counter)):
                    status = "Absent"
                    if is_in_counter[i] > 0:
                        status = "Present"

                    cv2.putText(frame, f'Cashier (Counter {is_in_counter[i]}): {status}',
                                (counter_info[i], 400), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

                for i in range(len(n_customers)):
                    status = "Free"
                    if n_customers[i] > 0:
                        status = "Occupied"

                    cv2.putText(frame, f'Customer (Counter {n_customers[i]}): {status}',
                                (customer_info[i], 500), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

                video_writer.write(frame)
                continue

            detections, tracks = tracker.update(boxes.copy(),
                                                scores.copy(),
                                                frame.copy(),
                                                update_fg)

            fps.update()
            fps.stop()

            for track in tracks:
                if not track.is_confirmed() or track.time_since_update > 1:  # > 1
                    continue

                track_centroids = track.get_centroids()
                for i in range(0, len(track_centroids) - 1):
                    # Draw trace line
                    x1, y1 = track_centroids[i][0], track_centroids[i][1]
                    x2, y2 = track_centroids[i + 1][0], track_centroids[i + 1][1]
                    clr = track.track_id % 9
                    cv2.line(frame, (int(x1), int(y1)), (int(x2), int(y2)),
                             track_colors[clr], 2)

            for detection in detections:
                alpha = 0.3
                frame_copy = frame.copy()
                detection = norm_box(detection)

                cv2.rectangle(frame, (int(detection[0]), int(detection[1])),
                              (int(detection[2]), int(detection[3])), colors[18], -1)
                cv2.addWeighted(frame, alpha, frame_copy, 1 - alpha, 0, frame_copy)

                frame = frame_copy.copy()
                cv2.rectangle(frame, (int(detection[0]), int(detection[1])),
                              (int(detection[2]), int(detection[3])), colors[18], 1)

                cv2.putText(frame, str(detection[4]),
                            (int(detection[0]), int(detection[1])), 0, 5e-3 * 80,
                            (0, 255, 0), 2)

            is_in_counter, n_customers = analyze_track(customer_regions[1:], counter_regions, detections)

            for i in range(len(is_in_counter)):
                status = "Absent"
                if is_in_counter[i] > 0:
                    status = "Present"

                cv2.putText(frame, f'Cashier (Counter {is_in_counter[i]}): {status}',
                            (counter_info[i], 400), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

            for i in range(len(n_customers)):
                status = "Free"
                if n_customers[i] > 0:
                    status = "Occupied"

                cv2.putText(frame, f'Customer (Counter {n_customers[i]}): {status}',
                            (customer_info[i], 500), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

            video_writer.write(frame)

        fps.stop()
        video_writer.release()
        stream.stop()
        cv2.destroyAllWindows()

        print("Elapsed time : {:.2f}".format(fps.elapsed()))
        print("Approx. FPS: {:.2f}".format(fps.fps()))
        print("Num frames: {:.2f} - {:.2f}".format(fps.num_frames, frame_counter))

    return service
