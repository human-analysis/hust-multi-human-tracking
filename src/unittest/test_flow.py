import warnings

warnings.filterwarnings("ignore")

import os
import unittest

from src.tracking.tracking_system import TrackingCreator
from src.utils.benchmark import benchmark_mAP


class Tester(unittest.TestCase):
    root_project = "/mnt/D466DA6A66DA4CBC/2020/schools/multi-human-tracking-ver1/"
    config_path = f"{root_project}/config/new_test.ini"

    def test_offline_flow(self):
        detect_freq = 2
        tracker = "DeepSort"

        demo = TrackingCreator("M2_SSD",
                               tracker,
                               self.config_path,
                               "Staple", "offline")

        track_scenario = f"M2_SSD_{tracker}_Staple_DetFreq{detect_freq}"
        inputs = os.path.join(self.root_project, "datasets/benchmark/test/sampled-PET/img/")
        demo.track_with_aux_tracker(inputs,
                   output_video=os.path.join(self.root_project,
                                             f"datasets/benchmark/results/{track_scenario}/sampled-PET/tracked-video.mp4"),
                   output_file=os.path.join(self.root_project,
                                            f"datasets/benchmark/results/{track_scenario}/sampled-PET/output.txt"),
                   num_frames=-1, detect_freq=detect_freq)

        gt_files = os.path.join(self.root_project, "datasets/benchmark/test/")
        ts_files = os.path.join(self.root_project, "datasets/benchmark/results/", track_scenario)
        benchmark_mAP(gt_files, ts_files, track_scenario, f"{self.root_project}/benchmark/final_mAP.txt")

        # deep sort 5.26 FPS
        # deep sort + mosse

    def test_offline_all(self):
        detect_freq = 3
        tracker = "LazyDeepSort"
        visual_tracker = "Dat"

        demo = TrackingCreator("M2_SSD",
                               tracker,
                               self.config_path,
                               visual_tracker, "offline")

        track_scenario = f"M2_SSD_{tracker}_{visual_tracker}_DetFreq{detect_freq}"
        seqs = os.listdir(os.path.join(self.root_project, "datasets/benchmark/data/"))
        for seq in seqs:
            input = os.path.join(self.root_project, f"datasets/benchmark/data/{seq}/img/")
            if visual_tracker == "":
                demo.track(input,
                           output_video=os.path.join(self.root_project,
                                                     f"datasets/benchmark/final_results/{track_scenario}/{seq}/tracked-video.mp4"),
                           output_file=os.path.join(self.root_project,
                                                    f"datasets/benchmark/final_results/{track_scenario}/{seq}/output.txt"),
                           num_frames=-1, detect_freq=detect_freq)
            else:
                demo.track_with_aux_tracker(input,
                                            output_video=os.path.join(self.root_project,
                                                                      f"datasets/benchmark/final_results/{track_scenario}/{seq}/tracked-video.mp4"),
                                            output_file=os.path.join(self.root_project,
                                                                     f"datasets/benchmark/final_results/{track_scenario}/{seq}/output.txt"),
                                            num_frames=-1, detect_freq=detect_freq)

        gt_files = os.path.join(self.root_project, "datasets/benchmark/data/")
        ts_files = os.path.join(self.root_project, "datasets/benchmark/final_results/", track_scenario)
        benchmark_mAP(gt_files, ts_files, track_scenario, f"{self.root_project}/benchmark/final_mAP.txt")
