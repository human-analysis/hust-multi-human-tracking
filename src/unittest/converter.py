import os
import unittest

import torch
from onnxruntime_tools import optimizer

from src.constant.detection import _C as default_cfg
from src.detection.modules.build import build_detection_model
from src.utils.checkpoint import CheckPointer


class Converter(unittest.TestCase):
    root = "/mnt/D466DA6A66DA4CBC/2020/schools/multi-human-tracking-ver1/"

    def test_convert_onnx16(self):
        optimized_model = optimizer.optimize_model(f"{self.root}/model_zoo/onnx/cpu_mobile_v2_ssd320.onnx",
                                                   model_type='gpt2', num_heads=12,
                                                   hidden_size=768, only_onnxruntime=True)
        optimized_model.convert_model_float32_to_float16()
        optimized_model.save_model_to_file(f"{self.root}/cpu_mobile_v2_ssd320_fps16.onnx")

    def test_convert_onnx(self):
        cfg = default_cfg

        model_name = "mobilenet_v2_ssd320"
        cfg.merge_from_file(
            f"{self.root}/config/model/{model_name}.yaml")
        checkpoint_file = os.path.join(self.root, "model_zoo/detection", f"{model_name}_repulsionfocalloss/model_final.pth")

        model = build_detection_model(cfg)
        checkpoint = CheckPointer(model, save_dir=cfg.OUTPUT_DIR)
        checkpoint.load(checkpoint_file, use_latest=checkpoint_file is None)

        model.eval()

        dummy_input = torch.randn((1, 3, 320, 320))
        torch.onnx.export(model, dummy_input,
                          f"{self.root}/model_zoo/onnx/cpu_{model_name}.onnx",
                          input_names=["inputs"],
                          opset_version=11)
        # print(model(dummy_input))
