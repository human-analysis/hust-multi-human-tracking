import os
import unittest

from src.constant.visualization import *
from src.utils.demo import get_crop_region
from src.utils.visualization import draw_polygon


class Visualizer(unittest.TestCase):
    root = "/mnt/D466DA6A66DA4CBC/2020/schools/multi-human-tracking-ver1/"
    src = os.path.join(root, "datasets/demo/retail.mp4")
    stream = cv2.VideoCapture(src)
    grabbed, frame = stream.read()
    stream.release()

    def test_poly_drawer(self):
        region = get_crop_region(self.frame)
        print(region)

    def test_show_region(self):
        cv2.imwrite("../../datasets/images/test_retail5.png", self.frame)
        draw_polygon(self.frame, interest_of_region)
        for poly in paying_regions:
            self.frame = draw_polygon(self.frame, poly)

        for poly in customer_regions:
            self.frame = draw_polygon(self.frame, poly)

        for i in range(len(paying_lines)):
            for line in paying_lines[i]:
                cv2.line(self.frame, (line[0][0], line[0][1]),
                         (line[1][0], line[1][1]), (0, 255, 0), 1)

        for point in outlier_points:
            cv2.circle(self.frame, (point[0], point[1]), radius=0, color=(0, 0, 255), thickness=-1)
            cv2.putText(self.frame, f'{(point[0], point[1])}',
                        (point[0] - 2, point[1] - 2), cv2.FONT_ITALIC, 0.3,
                        (255, 255, 0), 1)

        cv2.imshow("Poly_region", self.frame)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
