import os
import time
import unittest
from configparser import ConfigParser
from glob import glob
from io import BytesIO

import cv2
import numpy as np
import requests
import torch
from PIL import Image

from src.constant.detection import _C as default_cfg
from src.constant.visualization import interest_of_region, outlier_points, outlier_regions
from src.detection import get_detector
from src.detection.modules.build import build_detection_model
from src.utils.checkpoint import CheckPointer
from src.utils.geometry import is_inside_polygon
from src.utils.visualization import draw_boxes, draw_polygon


class Tester(unittest.TestCase):
    root = "/mnt/D466DA6A66DA4CBC/2020/schools/multi-human-tracking-ver1/"
    config_path = f"{root}/config/test.ini"
    assert os.path.exists(config_path) is True
    cfg = ConfigParser()
    cfg.read(config_path)

    def test_detector0(self):
        default_cfg.merge_from_file(
            "/mnt/D466DA6A66DA4CBC/2020/schools/multi-human-tracking-ver1/constant/model/mobilenet_v2_ssd320.yaml")
        pre_trained_file = f"/mnt/D466DA6A66DA4CBC/2020/schools/multi-human-tracking-ver1/" \
                           f"/model_zoo/detection/mobilenet_v2_ssd320_focalloss/model_final.pth"

        model = build_detection_model(default_cfg)

        checkpoint = CheckPointer(model, save_dir=default_cfg.OUTPUT_DIR)
        checkpoint.load(pre_trained_file, use_latest=pre_trained_file is None)

        inputs = torch.rand(2, 3, 320, 320)

        outputs = model(inputs)

    def test_detector1(self):
        frame_paths = glob(os.path.join(self.root, "datasets/benchmark/data/ADL-Rundle-6/img", "*.jpg"))
        detector = get_detector("M2_SSD", self.cfg, "onnx")

        frame = cv2.imread(frame_paths[20])
        boxes, scores = detector.detect_image(frame)
        idx = scores > 0.2
        draw_boxes(frame, boxes[idx], scores[idx], True, saving_file="../../datasets/images/sample1.png")

    def test_detector2(self):
        detector = get_detector("M2_SSD", self.cfg, "onnx")
        frame = cv2.imread(os.path.join(self.root, "datasets/images/test_retail5.png"))
        boxes, scores = detector.detect_image(frame, confidence=0.0)

        draw_polygon(frame, interest_of_region)

        for point in outlier_points:
            cv2.circle(frame, (point[0], point[1]), radius=1, color=(0, 255, 0), thickness=-1)
            cv2.putText(frame, f'{(point[0], point[1])}',
                        (point[0] - 2, point[1] - 2), cv2.FONT_ITALIC, 0.3,
                        (255, 0, 0), 1)

        tmp_boxes, tmp_scores = [], []

        for i, box in enumerate(boxes):
            centroid = [(box[0] + box[2]) / 2, (box[1] + box[3]) / 2]
            if is_inside_polygon(interest_of_region, centroid):
                if is_inside_polygon(
                        [[outlier_regions[0], outlier_regions[1]], [outlier_regions[2], outlier_regions[1]],
                         [outlier_regions[2], outlier_regions[3]], [outlier_regions[0], outlier_regions[3]]], centroid):
                    continue

                is_box = True
                for point in outlier_points:
                    if is_inside_polygon([[box[0], box[1]], [box[2], box[1]], [box[2], box[3]], [box[0], box[3]]],
                                         point):
                        is_box = False
                if is_box:
                    tmp_boxes.append(box)
                    tmp_scores.append(scores[i])
            cv2.circle(frame, (int(centroid[0]), int(centroid[1])), radius=1, color=(0, 0, 255), thickness=-1)

        boxes = np.array(tmp_boxes)
        scores = np.array(tmp_scores)
        draw_boxes(frame, boxes, scores, True)

    def test_detector3(self):
        # url_path = "http://s120.avatar.talk.zdn.vn/f/1/f/d/17/120/675c1cf0e99cba85ee4dd69f22f7aaab.jpg"
        url_path = "http://s120.avatar.talk.zdn.vn/0/0/e/4/65/120/df249295660cf5097e028cf5ecfd7bad.jpg"

        response = requests.get(url_path)
        image = Image.open(BytesIO(response.content))

        image = cv2.cvtColor(np.array(image), cv2.COLOR_RGB2BGR)
        image = cv2.resize(image, (600, 600))

        detector = get_detector("M2_SSD", self.cfg, "onnx")
        start = time.time()
        boxes, scores = detector.detect_image(image)
        print(time.time() - start)

        idx = scores > 0.4
        draw_boxes(image, boxes[idx], scores[idx], True)

    def test_detector4(self):
        hog = cv2.HOGDescriptor()
        hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())

        frame = cv2.imread(os.path.join(self.root, "datasets/images/retail.png"))
        (regions, _) = hog.detectMultiScale(frame,
                                            winStride=(4, 4),
                                            scale=1.05)

        boxes = [[region[0], region[1], region[0] + region[2],
                  region[1] + region[3]] for region in regions]
        scores = [1.0] * len(boxes)
        draw_boxes(frame, boxes, scores, True)

    def test_head_detector(self):
        cascade = cv2.CascadeClassifier(os.path.join(self.root, "model_zoo/opencv/cascadeH5.xml"))

        frame = cv2.imread(os.path.join(self.root, "datasets/images/test_retail4.png"))

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        blackwhite = cv2.equalizeHist(gray)

        start = time.time()
        regions = cascade.detectMultiScale(
            blackwhite, scaleFactor=1.1, minNeighbors=10, minSize=(40, 40),
            flags=cv2.CASCADE_SCALE_IMAGE)
        print(time.time() - start)

        boxes = [[region[0], region[1], region[0] + region[2],
                  region[1] + region[3]] for region in regions]
        scores = [1.0] * len(boxes)
        draw_boxes(frame, boxes, scores, True)
