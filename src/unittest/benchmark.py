import os
import unittest

from src.utils.benchmark import benchmark_mAP


class EngineTest(unittest.TestCase):
    root_project = "/mnt/D466DA6A66DA4CBC/2020/schools/multi-human-tracking-ver1/"

    def testMotEval(self):
        data_path = f"{self.root_project}/storage/"

        gt_files = os.path.join(data_path, "sample")
        ts_files = os.path.join(data_path, "results", "mobile_v2_ssd_dat_deep_skip3")
        benchmark_mAP(gt_files, ts_files, "mobile_v2_ssd_dat_deep_skip3", f"{self.root_project}/benchmark/mAP.txt")
