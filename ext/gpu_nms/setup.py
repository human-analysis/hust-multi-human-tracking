from setuptools import setup
from torch.utils.cpp_extension import CUDAExtension, BuildExtension

setup(name='gpu_extension',
      ext_modules=[CUDAExtension('gpu_nms', ['gpu_nms.cpp', 'nms_kernel.cu'])],
      cmdclass={'build_ext': BuildExtension})
